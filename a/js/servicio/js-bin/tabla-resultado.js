var $resultadosContainer = $("#resultados");
var $tabla = $resultadosContainer.find("#table");
var $modalRecibo = $resultadosContainer.find("#modalPreviewRecibo");
var $modalNC = $resultadosContainer.find("#modalPreviewNC");

var $reciboEmail = $("#reciboEmail");
var $NCEmail = $("#NCEmail");

$tabla.footable();

$resultadosContainer.on('click', '.preview-recivo', function (e) {
	$modalRecibo.modal('show');
	e.preventDefault();
});

$resultadosContainer.on('click', '.preview-nota', function (e) {
	$modalNC.modal('show');
	e.preventDefault();
});

var configValidate = _.extend(window._validatorWallSettings, {
	rules: {
		txtEmail: {
			email: true
		}
	},
    submitHandler: function () {
        console.log('sending email');
        // form.submit();
    }
});

$reciboEmail.validate(configValidate);
$NCEmail.validate(configValidate);