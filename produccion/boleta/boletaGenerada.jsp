<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Boleta Electr&oacute;nica</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/a/js/libs/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/a/js/libs/bootstrap/3.3.1/css/normalize.css">
    <link href="/a/js/libs/bootstrap/3.3.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/a/js/libs/bootstrap/3.3.1/css/footable.core.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>



<%@page import="pe.gob.sunat.servicio2.registro.electronico.comppago.boleta.bean.ComprobanteUtilBean"%>
<%@page import="pe.gob.sunat.servicio2.registro.electronico.comppago.boleta.bean.OtroDocumentoRelacionadoBean"%>
<jsp:useBean id="comprobante" scope="session" class="pe.gob.sunat.servicio2.registro.electronico.comppago.boleta.bean.ComprobanteBean"/>   



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Emisi&oacute;n de Boleta Electr&oacute;nica</h3> </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 ">
                                <div class="alert alert-warning" role="alert">
                                    <h4>  <strong> Boleta Electr&oacute;nica <%=comprobante.getSerieComprobante()%>-<%=comprobante.getNumeroComprobante()%></strong>



                                    	  <br>RUC: <%=comprobante.getNumeroRuc()%><br>	

                                    	  	 <%=comprobante.getSerieComprobante()%>-<%=comprobante.getNumeroComprobante()%>

                                    </h4> </div> <address>
                        
                  

                  <strong><%=comprobante.getRazonComercial()%></strong>

                  <br>

                  <strong><%=comprobante.getRazonSocial()%></strong>

                  <br>

                 <%=comprobante.getNombreCalle()%> <%=comprobante.getNumeroDireccion()%>

                  <br>

                  <%=comprobante.getNombreDistrito()%> - <%=comprobante.getNombreProvincia()%> - <%=comprobante.getNombreDepartamento()%>

                 </address>
                                <hr> <strong>Fecha de Emisi&oacute;n:</strong>
                                <br> <%=comprobante.getFechaEmision()%>
                                <br> <strong>Raz&oacute;n Social </strong>
                                <br> <%=comprobante.getNombreCliente()%>
                                <br> <strong> <%=comprobante.getDesTipoDocumentoCliente()%> </strong>
                                <br> <%=comprobante.getNumeroDocumentoCliente()%>
                                <br> <strong>Tipo de Moneda</strong>
                                <br> <%=comprobante.getDescripcionMoneda()%>
                                <br> <strong>Lugar de Entrega</strong>
                                <br> 
                                <br>
                                <br>
                                <div class="alert alert-warning" role="alert">
                                    <h4><strong>Documentos Relacionados</strong></h4>
                                </div>



                                <% List relacionados = comprobante.getOtroDocumentoRelacionadoBean();

                                if (relacionados.size()>0) {

                                	for( OtroDocumentoRelacionadoBean  doc : comprobante.getOtroDocumentoRelacionadoBean() ) {%>

                                   <br>                           
                               <strong> <%=doc.getDesTipoDocuRela()%></strong>
                               <br><%=doc.getSerieDocumentoRelacionado()%> - <%=doc.getNumeroDocumentoRelacionadoInicial() %> 
     
                               <%}
                               } %> 

                              
                                <br>
                                <table class="table table-striped table-bordered footable" id="tabla" data-page-size="5">
                                    <thead>
                                        <tr>
                                            <th data-hide="phone">Cantidad</th>
                                            <th data-hide="phone">Unidad de Medida</th>
                                            <th>C&oacute;digo</th>
                                            <th>Descripci&oacute;n</th>
                                            <th data-hide="phone, tablet">Valor Unitario</th>
                                            <th data-hide="phone, tablet">Descuento</th>
                                            <th data-hide="phone">Igv </th>
                                            <th data-hide="phone">Importe del Item</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                      
                                    	   <% for(DetalleComprobanteBean detalle : comprobante.getDetalleComprobanteBean()) { %>


                                        <tr>
                                            <td><%=detalle.getCantidad()%></td>
                                            <td><%=detalle.getUnidadMedidaDesc()%></td>
                                            <td><%=detalle.getCodigoItem()%></td>
                                            <td><%=detalle.getDescripcion()%></td>
                                            <td><%=detalle.getPrecioUnitarioAux()%></td>
                                            <td><%=detalle.getDescuentoMonto()%></td>
                                            <td><%=detalle.getIgvMonto()%></td>
                                            <td><%=detalle.getImporteVenta()%></td>
                                        </tr>

                                           <%} %>


                                    </tbody>
                                </table>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"> </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <form class="form-horizontal">
                                                    <div class="form-group"> <strong class="col-sm-10 text-right">Total Valor Venta Gravado</strong>
                                                        <div class="visible-xs">
                                                            <br>
                                                        </div> <span class="col-sm-2" id="total-valor-venta-grabado"><%=comprobante.getSimboloMoneda()%>&nbsp;<%=ComprobanteUtilBean.formatTwoDecimalComa(comprobante.getTotalValorVenta())%></span> </div>
                                                    <div class="form-group"> <strong class="col-sm-10 text-right">Total Valor Descuentos</strong>
                                                        <div class="visible-xs">
                                                            <br>
                                                        </div> <span class="col-sm-2" id="total-valor-descuentos"><%=comprobante.getSimboloMoneda()%>&nbsp;<%=ComprobanteUtilBean.formatTwoDecimalComa(comprobante.getTotalDescuentos())%></span> </div>
                                                    <div class="form-group"> <strong class="col-sm-10 text-right">Total IGV (18%)</strong>
                                                        <div class="visible-xs">
                                                            <br>
                                                        </div> <span class="col-sm-2" id="total-igv"><%=comprobante.getSimboloMoneda()%>&nbsp;<%=ComprobanteUtilBean.formatTwoDecimalComa(comprobante.getTotalIGV())%></span> </div>
                                                    <div class="form-group"> <strong class="col-sm-10 text-right">Importe Total</strong>
                                                        <div class="visible-xs">
                                                            <br>
                                                        </div> <span class="col-sm-2" id="importe-total"><%=comprobante.getSimboloMoneda()%>&nbsp;<%=ComprobanteUtilBean.formatTwoDecimalComa(comprobante.getMontoTotalGeneral())%></span> </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div> 



                                <a href="emitirbvsimp.do" class="btn btn-danger" role="button">Volver</a> <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">Enviar Por correo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    



     <!-- Modal enviar por correo -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Enviar Correo</h4> </div>
                <div class="modal-body">
                    <form  id="form-email" method="post" action="factura-generado.html" name="form-email" >
                        <div class="form-group">
                            <label for="exampleInputEmail1"  class="hidden-xs">Envio a correo electr&oacute;nico</label>
                            <input type="text" class="form-control required email"  data-msg-required="Verifique su email" data-msg-email="Verifique su email" id="txtEmail" placeholder="Ingrese su Correo"> 
                        </div>
                        
                        <label for="exampleInputEmail1">El correo electr&oacute;nico adjuntar&aacute; el PDF y el XML</label>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Volver</button>
                    <input type="submit" class="btn btn-primary" value="Enviar">

                </div>
                </form>
            </div>
        </div>
    </div>

    <script src="/a/js/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="/a/js/libs/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="/a/js/servicio/js-bin/footable.min.js"></script>
    <script src="/a/js/servicio/js-bin/footable.paginate.js"></script>
    <script src="/a/js/servicio/js-bin/jquery.validate.js"></script>
    <script src="/a/js/servicio/js-bin/underscore.js"></script>
    <script src="/a/js/servicio/js-bin/normalization.js"></script>
    <script src="/a/js/servicio/js-bin/form-factura-generado.js"></script>
</body>

</html>