//(function(window, $, _){
	/*
	* :::: CONTAINERS :::: START
	*/
	var $formSimple = $("#form-busqueda");
	var $formAvanzada = $("#form-busqueda-avanzada");
	/*
	* :::: CONTAINERS :::: / END
	*/

	/*
	* :::: ELEMENTOS CRUCIALES (mas que todo, form-controls) :::: START
	*/

	// integridad y consistencia

	var $tipoCompField = $formSimple.find("#selCod_comprob");
	var $numSerieField = $formSimple.find('#txtNum_serie');
	var $numCompField = $formSimple.find('#txtNum_comprob');
	
	
	var $numDocField = $formAvanzada.find('#txtNumdocide');
	var $tipoDocField = $formAvanzada.find("#selCod_docide");

	var $desdeField = $formAvanzada.find('#txtFec_desde');
	var $hastaField = $formAvanzada.find('#txtFec_hasta');
	
	var $checkRecibo = $formAvanzada.find('#chkRecibo');
	var $checkReciboFisico = $formAvanzada.find('#chkReciboFisico');
	var $checkNota = $formAvanzada.find('#chkNota');
	var $checkNotaFisica = $formAvanzada.find('#chkNotaFisica');
	var $checkOtros = $formAvanzada.find('#chkOtros');

	var $_COMP_VALID = $formAvanzada.find('#_COMP_VALID');
	var $_helperDocField = $formAvanzada.find('#helperDoc');
	var $modalPreloader = $formAvanzada.find('#modalPreloader');
	var $modalDatepicker = $formAvanzada.find('#modalDatepicker');

var $nombreClienteField = 0;


	var $btnSimple = $('#btnSimple');
	var $btnAvanzado = $('#btnAvanzado');
	
	var $btnSubmitAdv = $formAvanzada.find('#enviar_avanzado');


    var $containerAvanzado = $('#form2');
    var $containerSimple = $('#form1');




	var movilTextoComp = $('#txtMovilComp').text();

	/*
	* :::: ELEMENTOS CRUCIALES (mas que todo, form-controls) ::::  / END
	*/

	/*
	* :::: PLUGIN SETUP ::::  START
	*/
	var _minDate = new Date('2005-01-01 00:00');

	var des = $modalDatepicker.find("#datepicker-desde");
	des.datepicker({
		endDate: new Date(),
		startDate: new Date('2005-01-01 00:00')
	}).on('changeDate', function (e) {
		$desdeField.val([e.date.getDate() < 10 ? '0'+(e.date.getDate()) : e.date.getDate(), e.date.getMonth() < 9 ? '0'+(e.date.getMonth()+1) : (e.date.getMonth()+1), e.date.getFullYear()].join('/'));
		ajustarFechaMaxima(e.date);
		$modalDatepicker.modal('hide');
	});
	var has = $modalDatepicker.find("#datepicker-hasta");

	has.datepicker({
		endDate: new Date(),
		startDate: new Date('2005-01-01 00:00')
	}).on('changeDate', function (e) {
		$hastaField.val([e.date.getDate() < 10 ? '0'+(e.date.getDate()) : e.date.getDate(), e.date.getMonth() < 9 ? '0'+(e.date.getMonth()+1) : (e.date.getMonth()+1), e.date.getFullYear()].join('/'));
		ajustarFechaMinima(e.date);
		$modalDatepicker.modal('hide');
	});

	function ajustarFechaMaxima(selectedDate) {
		
		var priorDate = new Date(selectedDate);
		var today = new Date()
		

		if(priorDate.getMonth() < 6){
			priorDate.setMonth(priorDate.getMonth() + 6);
		}else{
			priorDate.setFullYear(priorDate.getFullYear() + 1);
			priorDate.setMonth(priorDate.getMonth() - 6);
		}

		
		if(priorDate.getTime() > today.getTime())
			priorDate.setTime(today.getTime());

		//selectedDate.setTime(selectedDate.getTime()+86400000);

		has.data('datepicker').setStartDate(selectedDate);
		has.data('datepicker').setEndDate(priorDate);

	}

	function ajustarFechaMinima(selectedDate) {
		
		var priorDate = new Date(selectedDate);

		if(priorDate.getMonth() > 5){
			priorDate.setMonth(priorDate.getMonth() - 6);
		}else{
			priorDate.setFullYear(priorDate.getFullYear() - 1);
			priorDate.setMonth(priorDate.getMonth() + 6);
		}

		if(priorDate.getTime() < _minDate.getTime())
			priorDate.setTime(_minDate.getTime());

		
		selectedDate.setTime(selectedDate.getTime()-86400000);

		des.data('datepicker').setEndDate(selectedDate);
		des.data('datepicker').setStartDate(priorDate);
	}



	$btnSimple.bind('click', function(e) {
        
        $containerAvanzado.addClass('hidden');
        $containerSimple.removeClass('hidden');
        $btnSimple.removeClass('btn btn-default');
        $btnSimple.addClass('btn btn-primary');

          $btnAvanzado.removeClass('btn btn-primary');
        $btnAvanzado.addClass('btn btn-default');


        
        console.log('activo');
    });


	$btnAvanzado.bind('click', function(e) {

        $containerSimple.addClass('hidden');
        $containerAvanzado.removeClass('hidden');
        $btnAvanzado.removeClass('btn btn-default');
        $btnAvanzado.addClass('btn btn-primary');
        $btnSimple.removeClass('btn btn-primary');
        $btnSimple.addClass('btn btn-default');
        

        console.log('activo');
    });




	$btnSubmitAdv.on('click', function  (e) {
		$('.has-error').removeClass('has-error');
		/****/
		if(($tipoDocField.val() == "1" || $tipoDocField.val() == "6"))
			ValidaDniRucExistencia();
		else
		/****/
			$formAvanzada.submit();
	})



/*

	$desdeField.on('change', function (e) {
		var selectedDate = new Date(e.target.value.split('/').reverse().join('-') + ' 00:00');
		var priorDate = new Date(selectedDate);
		var today = new Date()
		priorDate.setFullYear(priorDate.getFullYear() + 1);
		if(priorDate.getTime() > today.getTime())
			priorDate.setTime(today.getTime());

		$hastaField.data('datepicker').setStartDate(selectedDate);
		$hastaField.data('datepicker').setEndDate(priorDate);
	});

	$hastaField.on('change', function (e) {
		var selectedDate = new Date(e.target.value.split('/').reverse().join('-') + ' 00:00');
		var priorDate = new Date(selectedDate);
		priorDate.setFullYear(priorDate.getFullYear() - 1);

		if(priorDate.getTime() < _minDate.getTime())
			priorDate.setTime(_minDate.getTime());

		$desdeField.data('datepicker').setEndDate(selectedDate);
		$desdeField.data('datepicker').setStartDate(priorDate);
	});


*/


// $desdeField.on('change', );

	// $hastaField.on('change', );



	/*
	* :::: PLUGIN SETUP ::::  / END
	*/


	/*
	* :::: FUNCIONES ::::  START
	*/
		function validTipoComp () {
			var valid = $checkRecibo.prop('checked')
			  | $checkReciboFisico.prop('checked')
			  | $checkNota.prop('checked')
			  | $checkNotaFisica.prop('checked')
			  | $checkOtros.prop('checked');

			 console.log(valid);
			$_COMP_VALID.val(valid ? '1' : '');
		}

		function ValidaDniRucExistencia(){
			console.log('validando')
			if(!$numDocField.val()){
				$numDocField.data('pivot-valid', '0');
				$formAvanzada.valid();
				return;
			}

			var formData = $formAvanzada.serializeObject();
			 var url = 'consemisor.do?action=validarDocumento'; 

			//var url = ['../json/',$numDocField.val(),'.json'].join('');
			$modalPreloader.modal('show');
			console.log('modal-shown');
			$.ajax({
	            url: url,
	            type: "POST",
	            data: formData,
	            dataType: 'json',
	            success: function(data) {
	            	console.log('got response', data);
	            	if(data.nombrecliente == '0'){ 
	            		$numDocField.data('pivot-valid', '0');
	            		return;	
	            	}
	            	$numDocField.data('pivot-valid', '1');
	            },
	            error: function () {
	            	console.log('got no response');
	            	$numDocField.data('pivot-valid', '0');
	        		return;
	            },
	            complete: function () {
	            	console.log('done');
	            	if($formAvanzada.valid())
	            		$formAvanzada.submit();
	            	$modalPreloader.modal('hide');
	            }
	        });
		}
	/*
	* :::: FUNCIONES ::::  / END
	*/


	/*
	* :::: HANDLERS ::::  START
	*/

	$('.datepicker-modal').on('click', function (e) {
		var $self = $(e.target).closest('.datepicker-modal');
		des.toggleClass('hidden', !$self.hasClass('desde'));
		has.toggleClass('hidden', !$self.hasClass('hasta'));
		$modalDatepicker.modal('show');

	});

	$tipoCompField.on('change', function (e, ignoreChange) {
		var serie;
		var electronicos = ["1","3","5"];
		if(electronicos.indexOf(e.target.value) !== -1){
			serie = 'E'
		}else{
			serie = '0'
		}
		
		serie += '001';

		$numSerieField.val(serie);
		$numCompField.focus();

	});

	$tipoDocField.on('change', function (e) {
		$('.has-error').removeClass('has-error');
		if(e.target.value == ""){
			$numDocField.val('').attr('readonly', 'readonly');
		}else{
			$numDocField.removeAttr('readonly');
		}

		$numDocField.val('');
		var length = 12;

		if(e.target.value == "6")
			length = 11;
		if(e.target.value == "1")
			length = 8;
		$numDocField.attr('maxlength', length);

		$formAvanzada.find('.lista-errores').remove();
	})

	// saca todos los checkboxes del div que los contiene
	// mas sencillo que declarar un handler separado por checkbox
	// que al final llamará a la misma función.-

	$formAvanzada.find('#checks-comprobante').find('input').on('change', function () {
		validTipoComp();
	})

	$numCompField.on('keyup', function (e) {
		e.target.value = e.target.value.replace(/\D/g, '');
	})

	/*
	$numDocField.on('change', function () {
		
		
		$_helperDocField.val('1');
		$formAvanzada.valid();

		if(!($tipoDocField.val() == "1" || $tipoDocField.val() == "6"))
			return;

		
		if($numDocField.data('validRUC') == '1' || $tipoDocField.val() == "1"){
			$modalPreloader.modal('show');
			ValidaDniRucExistencia();
		}
	});
*/

	


	/*
	* :::: HANDLERS :::: / END
	*/

	/*
	* :::: FORM VALIDATE ::::  START
	*/

	$formSimple.validate(_.extend(window._validatorWallSettings, {
		debug: true,
		rules: {
			txtNum_serie: {


				 digits: {
                    depends: function() {
                        return $("#selCod_comprob").val() ==2 || $("#selCod_comprob").val() ==4  ;
                    }
                },
				
				non_repeated: {
                    depends: function() {
                        return $("#selCod_comprob").val() ==2 || $("#selCod_comprob").val() ==4  ;
                    }
                },

	            num_serie_doc: true,
	            required: {
	            	depends: function () {
	            		return $tipoCompField.val() != "";
	            	}
	            },


	         



	        },
	        txtNum_comprob: {
	            digits: true,
	            required: {
	            	depends: function () {
	            		return $tipoCompField.val().trim() != "";
	            	}
	            }
			}
		},
		ignore: '',
		// estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
		messages:{
			txtNum_serie: {
				num_serie_doc: function (e, element) {
					var value = $numSerieField.val();
					if(value[0] == 'E' && value != 'E001'){
						return "El numero de serie debe ser E001 para los comprobantes electronicos"
					}
					return "El numero de serie debe ser s&oacute;lo d&iacute;gitos o E001 para los comprobantes electronicos"
				}
			}		
		},
		submitHandler: function (form) {
			form.submit();
			

			/*
			var url = '';
			$.ajax({
				method: 'post',
				dataType: 'json',
				complete: function () {
					window.location.href = "tabla-resultado.html"
				}
			})

			*/
		}
	}));

	$formAvanzada.validate(_.extend(window._validatorWallSettings, {
		debug: true,
		   onkeyup: false,
        onchange: false,
        onclick: false,
        onfocusout: false,
		rules: {
			txtFec_desde: {
				required: {
					depends: function () {
						return $tipoDocField.val() == "";
					}
				}
			},
			txtFec_hasta: {
				required: {
					depends: function () {
						return $tipoDocField.val() == "";
					}
				}
			},
			txtNumdocide: {
				required: {
					depends: function (element) {
						return $tipoDocField.val() != "";
					}
				},
				non_repeated: {
					depends: function (element) {
						return $tipoDocField.val() != "" && ($tipoDocField.val() == "6" || $tipoDocField.val() == "1");
					}
				},
				validStr: {
					depends: function (element) {
						return $tipoDocField.val() != "";
					}
				},
				validRUC: {
					depends: function (element) {
						return $tipoDocField.val() != "" && $tipoDocField.val() == "6";
					}
				},
				digits: {
					depends: function (element) {
						return $tipoDocField.val() != "" && ($tipoDocField.val() == "1" || $tipoDocField.val() == "6");
					}
				},
				validpivot:{
					depends: function (element) {
						return ($tipoDocField.val() == "1" && ($tipoDocField.val() == "6") && $numDocField.val() != "");
					}
				}
			}
		},
		ignore: '',
		// estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
		messages:{
			txtNumdocide: {
				notEqual: function () {
					if($tipoDocField.val() == "6")
						return "El usuario del servicio debe ser diferente al emisor"
					if($tipoDocField.val() == "1")
						return "El usuario del servicio debe ser diferente al emisor"
					return "not-equal";
				},
				digits: function () {
					if($tipoDocField.val() == "6")
						return "El numero de RUC no es valido"
					if($tipoDocField.val() == "1")
						return "El documento ingresado no existe"
					return "digits";	
				},
				required: function () {
					if($tipoDocField.val() == "6")
						return "El numero de RUC no es valido"
					if($tipoDocField.val() == "1")
						return "El documento ingresado no existe"
					return "Ingrese Documento";
				},
				validpivot : function () {
					if($tipoDocField.val() == "6")
						return "El numero de RUC no es valido";
					if($tipoDocField.val() == "1")
						return "El documento ingresado no existe"
				}
			}
		},
		submitHandler: function (form) {
			
			form.submit();
			/*
			var url = '';
			$.ajax({
				method: 'post',
				dataType: 'json',
				complete: function () {
					window.location.href = "tabla-resultado.html"
				}
			})

				*/
		}
	}));

	/*
	* :::: FORM VALIDATE :::: / END
	*/
	if($('.hidden-xs').not(':visible').length > 0)
		$tipoCompField.find('option:first').text(movilTextoComp)

	$tipoCompField.find('option:first')
	$tipoCompField.val("1").trigger('change', true);
	$numCompField.focus();

//})(window, jQuery, _);