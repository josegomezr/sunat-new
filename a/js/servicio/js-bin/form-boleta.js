// (function (window, $, _) {
    // avance al 08-09-2015 @ 22:48 VET
    /*
     * :::: CONTAINERS :::: START
     */
    var $formCatalogo = $("#form-catalogo");
    var $txtRazonSocial = $("#txtRazonSocial");
    var $selTipoDoc = $("#selTipoDocumento");
    var $txtLugarEntrega = $("#txtLugarEntrega");
    var $txtFechaVencimiento = $("#txtFechaVencimiento");
    var $txtIndicador = $("#txtIndicador");
    var $modalDirecciones = $("#modal-direcciones");
    var $tablaDireccionesReceptor = $modalDirecciones.find("#tabla-direcciones-receptor");
    var $tablaDocumento = $("#tabla-documento-relacionado");
    var $tablaItem = $("#tabla-item");
    var $modalListaDocumentoRelacionado = $("#modal-mostrar-documento-relacionado")
    var $modalListaItems = $("#modal-mostrar-items")
    var $modalDocumentoRelacionado = $("#modal-documento-relacionado")
    var $formModalDocumentoRelacionado = $modalDocumentoRelacionado.find('#form-modal-documento-relacionado');
    var $selTipoDocumentoRelacionado = $formModalDocumentoRelacionado.find('#selTipoDocumentoRelacionado');
    var $txtSerie = $formModalDocumentoRelacionado.find('#txtSerie');
    var $modalItem = $("#modal-item")
    var $formModalItem = $modalItem.find('#form-modal-item');
    var $txtCodigo = $formModalItem.find('#txtCodigo');
    var $txtDescripcion = $formModalItem.find('#txtDescripcion');
    var $selUnidadMedida = $formModalItem.find('#selUnidadMedida');
    var $selTipo = $formModalItem.find('#selTipo');
    var $btnOpenModalDR = $('#open-modal-documento-relacionado');
    var $btnOpenModalItem = $('#open-modal-item');
    var $btnOpenModalSearch = $('#open-modal-search');
    var $modalSearch = $("#modal-search");
    var $tablaProducto = $modalSearch.find("#tabla-producto");
    var $formSearch = $modalSearch.find('#form-modal-search');
    var $txtProductQuery = $formSearch.find('#product-query');
    var $lblTotalVentaGravado = $('#total-valor-venta-gravado');
    var $lblTotalValorDescuentos = $('#total-valor-descuentos');
    var $lblTotalTotalIGV = $('#total-igv');
    var $lblTotalImporteTotal = $('#importe-total');
    var $modalDatepicker = $('#modal-datepicker');
    var $datepicker = $modalDatepicker.find("#datepicker-placeholder");
    var $datepickerHidden = $modalDatepicker.find("#datepicker-value");
    var $btnCloseDatepicker = $modalDatepicker.find('#modal-datepicker-close');
    var $formBoletaDatosReceptor = $('#form-boleta-datos-receptor');
    var $RUCField = $('#txtRuc, #txtDocumento');
    var $razonSocial = $('#txtRazonSocial');
    var $btnValidarDatosReceptor = $('#btnValidarDatosReceptor');
    var $btnContinuar = $('#btnContinuarPaso');
    var $modalPreloader = $('#modalPreloader');
    var $panelBoletaDatosReceptor = $('#panel-boleta-datos-receptor');
    var $panelDatosComprobante = $('#panel-datos-comprobante');
    var $panelDocumentosRelacionados = $('#panel-documentos-relacionados');
    var $panelItems = $('#panel-items');
    var $panelResumen = $('#panel-resumen');
    var $btnMostrarModalDR = $panelDocumentosRelacionados.find('#btnMostrarModalDR');
    var $btnMostrarModalItems = $panelItems.find('#btnMostrarModalItems');
    var $selMoneda = $panelDatosComprobante.find('#selMoneda');
    var $btnPreview = $('#btnPreview');
    var $btnVolverBoleta = $('#btnVolverBoleta');
    var $hiddenRucEmisor = $("#rucEmisor");
    var $hiddenDireccionEmisor = $("#direccionEmisor");
    var $hiddenRazonSocialEmisor = $("#razonSocialEmisor");
    var $hiddenNombreComercialEmisor = $("#nombreComercialEmisor");
    var $hiddenDepartamentoEmisor = $("#departamentoEmisor");
    var $formItem = $("#form-item")
    var $hiddenHelperItem = $formItem.find("#hiddenHelperItem");
    var $formDR = $("#form-modal-documento-relacionado")
    var $hiddenHelperDR = $formDR.find("#hiddenHelperDR");
    var $formComprobante = $("#form-datos-comprobante")
    var $btnEmitirBoleta = $("#btnEmitirBoleta");
    var serie1='EG01';
    var serie2='T001';
    
    /* 
     *  :::: CONTAINERS :::: END
     */
    /*
     *  :::: HELPERS :::: START
     */
    var _tipoDocumentoIdentidad = {
        "1": "DNI",
        "2": "PASAPORTE",
        "3": "CARNET DE EXTRANJERIA",
        "4": "RUC",
        "5": "SIN DOCUMENTO",
        "6": "CED DIPLOMATICA DE IDENTIDAD",
        "7": "DOC TRIB NO DOM SIN RUC",
    }
    var _tipoDocumentoRelacionado = {
        '09': 'Guia de Remisi&oacute;n',
        '31': 'Guia de Remisi&oacute;n de Transporte',
        '00': 'Otros',
    }
    var _tipoMedida = {
        '4A': 'BOBINAS',
        'BE': 'FARDO',
        'BG': 'BOLSA',
        'BJ': 'BALDE',
        'BLL': 'BARRILES',
        'BO': 'BOTELLAS',
        'BX': 'CAJA',
        'C62': 'PIEZAS',
        'CA': 'LATAS',
        'CEN': 'CIENTO DE UNIDADES',
        'CJ': 'CONOS',
        'CMK': 'CENTIMETRO CUADRADO',
        'CMQ': 'CENTIMETRO CUBICO',
        'CMT': 'CENTIMETRO LINEAL',
        'CT': 'CARTONES',
        'CY': 'CILINDRO',
        'DR': 'TAMBOR',
        'DZN': 'DOCENA',
        'DZP': 'DOCENA POR 0**6 ',
        'FOT': 'PIES',
        'FTK': 'PIES CUADRADOS',
        'FTQ': 'PIES CUBICOS',
        'GLI': 'GALON INGLES (4,545956L)',
        'GLL': 'US GALON (3,7843 L)',
        'GRM': 'GRAMO',
        'GRO': 'GRUESA',
        'HLT': 'HECTOLITRO',
        'INH': 'PULGADAS',
        'KGM': 'KILOGRAMO',
        'KT': 'KIT',
        'KTM': 'KILOMETRO',
        'KWH': 'KILOVATIO HORA',
        'LBR': 'LIBRAS',
        'LEF': 'HOJA',
        'LTN': 'TONELADA LARGA',
        'LTR': 'LITRO',
        'MGM': 'MILIGRAMOS',
        'MLL': 'MILLARES',
        'MLT': 'MILILITRO',
        'MMK': 'MILIMETRO CUADRADO',
        'MMQ': 'MILIMETRO CUBICO',
        'MMT': 'MILIMETRO ',
        'MTK': 'METRO CUADRADO',
        'MTQ': 'METRO CUBICO',
        'MTR': 'METRO',
        'MWH': 'MEGAWATT HORA',
        'NIU': 'UNIDAD',
        'ONZ': 'ONZAS',
        'PF': 'PALETAS',
        'PG': 'PLACAS ',
        'PK': 'PAQUETE',
        'PR': 'PAR',
        'RM': 'RESMA',
        'SET': 'JUEGO',
        'ST': 'PLIEGO',
        'STN': 'TONELADA CORTA',
        'TNE': 'TONELADAS',
        'TU': 'TUBOS',
        'UM': 'MILLON DE UNIDADES',
        'YDK': 'YARDA CUADRADA',
        'YRD': 'YARDA',
        'ZZ': 'UNIDAD'
    }
    var _moneda = {
        'USD': '&dollar;',
        'PEN': 'S/.',
        'EUR': '&euro;',
        'GBP': '&pound;',
        'JPY': '&yen;',
        'SEC': 'kr',
        'CHF': 'CHF',
        'CAD': 'C&dollar;',
    }
    var _monedaDescripcion = {
        'PEN': 'NUEVOS SOLES',
        'EUR': 'EUROS',
        'USD': 'DOLAR AMERICANO',
        'GBP': 'LIBRA ESTERLINA',
        'JPY': 'YEN',
        'SEC': 'CORONA SUECA',
        'CHF': 'FRANCO SUIZO',
        'CAD': 'DOLAR CANADIENSE'
    }
    var _tipoProducto = {
        'TI01': 'Bien',
        'TI02': 'Servicio'
    }
    var _itemEditando = false;
    var _DocumentosRelacionadosSchema = {
        selTipoDocumentoRelacionado: "Tipo de Documento",
        txtSerie: "Serie",
        txtNumero: "N&uacute;mero",
        //txtDescripcion: "Descripci&oacute;n"
    }
    var _DocumentosRelacionados = [];
    var _Items = [];
    var _DireccionesReceptor = [];
    var _DireccionesReceptorSchema = {
        "tipo": "Tipo de Domicilio",
        "domicilio": "Domicilio",
    };
    var _direccionSeleccionada = -1;
    var _itemSchema = {
        txtCantidad: "Cantidad",
        selTipo: "Tipo",
        selUnidadMedida: "Unidad de Medida",
        txtCodigo: "C&oacute;digo",
        txtDescripcion: "Descripci&oacute;n",
        txtValorUnitario: "Valor Unitario",
        txtDescuento: "Descuento",
        txtIGV: "IGV",
        txtImporte: "Importe",
    }
    var _Productos = [];
    var _ProductoSchema = {
        txtCodigo: "C&oacute;digo",
        txtDescripcion: "Descripci&oacute;n",
        selTipo: "Tipo",
        selUnidadMedida: "Unidad de Medida",
        selMoneda: "Moneda",
        txtValorUnitario: "Precio de Venta",
    }
    var _datosEnvio = {}
        /*
         *  :::: HELPERS :::: END
         */
        /*
         *  :::: FUNCTION :::: START
         */
    function renderDocumentoRelacionado() {
        var tbody = $tablaDocumento.find('tbody');
        tbody.empty();
        for (var i in _DocumentosRelacionados) {
            var doc = _DocumentosRelacionados[i];
            var $row = $('<tr/>', {
                'data-row-id': doc.rowId
            });
            for (var k in _DocumentosRelacionadosSchema) {
                var text = doc[k];
                if (k == 'selTipoDocumentoRelacionado') {
                    text = _tipoDocumentoRelacionado[text];
                }
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>').addClass('btn btn-danger btn-sm remover').append($('<i/>').addClass('glyphicon glyphicon-remove'))).appendTo($row);
            $row.appendTo(tbody);
        }
        $("#documentos-relacionados-counter").text(_DocumentosRelacionados.length)
        $hiddenHelperDR.val(_DocumentosRelacionados.length);
        $tablaDocumento.trigger('footable_initialize');
    }

    function agregarDocumentoRelacionado(data) {
        _DocumentosRelacionados.push(data);
        renderDocumentoRelacionado();
    }

    function eliminarDocumentoRelacionado(criteria) {
        _DocumentosRelacionados = _.reject(_DocumentosRelacionados, criteria)
        renderDocumentoRelacionado();
    }

    function editarDocumentoRelacionado(criteria, data) {
        var doc = _.find(_DocumentosRelacionados, criteria)
        doc = _.extend(doc, data);
        renderDocumentoRelacionado();
    }

    function renderItem() {
        var tbody = $tablaItem.find('tbody');
        tbody.empty();
        for (var i in _Items) {
            var doc = _Items[i];
            var $row = $('<tr/>', {
                'data-item-id': doc.itemId
            });
            for (var k in _itemSchema) {
                var text = doc[k];
                if (k == 'selUnidadMedida') {
                    text = _tipoMedida[text];
                }
                if (k == 'selTipo') continue;
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>').addClass('btn btn-danger btn-sm remover').append($('<i/>').addClass('glyphicon glyphicon-remove'))).appendTo($row);
            $row.appendTo(tbody);
        }
        $("#items-counter").text(_Items.length)
        $modalItem.find('.lista-errores').empty();
        $panelItems.find('.lista-errores').empty();
        $hiddenHelperItem.val(_Items.length);
        $tablaItem.trigger('footable_initialize');
    }

    function agregarItem(data) {
        _Items.push(data);
        renderItem();
    }

    function eliminarItem(criteria) {
        _Items = _.reject(_Items, criteria)
        renderItem();
    }

    function buscarItem(criteria) {
        return _.find(_Items, criteria);
    }

    function editarItem(criteria, data) {
        var doc = buscarItem(criteria);
        doc = _.extend(doc, data);
        renderItem();
    }

    function popularModalItem(data, triggerChange, ignoreCodigo) {
        for (var i in _itemSchema) {
            if (!data.hasOwnProperty(i) || ignoreCodigo && i == 'txtCodigo') continue;
            var el = $formModalItem.find('#' + i);
            el.val(data[i]);
            if (triggerChange) el.trigger('keyup').trigger('change');
        }
        if (data.hasOwnProperty('selMoneda')) {
            var totales = data.selMoneda;
            var divisa = $selMoneda.val()
            if (totales.hasOwnProperty(divisa)) {
                $formModalItem.find('#txtValorUnitario').val(totales[divisa]);
                if (triggerChange) $formModalItem.find('#txtValorUnitario').trigger('keyup').trigger('change');
            }
        }
    }

    function prefetchProductos() {
        // $.getJSON('emitirbvsimp.do?action=getCatalogoProductos', function(data) {
        $.getJSON('a/js/json/catalogo.json', function(data) {
            _Productos = data;
            $selMoneda.trigger('focus').trigger('change')
        })
    }

    function filtarProductos(descripcion) {
        var regex = new RegExp(descripcion, "ig");
        return _.filter(_Productos, function (curr) {
            return curr.txtDescripcion.match(regex) !== null;
        });
    }

    function renderProductos(productos) {
        productos = productos || _Productos;
        var tbody = $tablaProducto.find('tbody');
        tbody.empty();
        for (var i in productos) {
            var doc = productos[i];
            var $row = $('<tr/>', {
                'data-p-id': doc.pId
            });
            for (var k in _ProductoSchema) {
                var text = doc[k];
                if (k == 'selUnidadMedida') {
                    text = _tipoMedida[text];
                }
                if (k == 'selTipo') {
                    text = _tipoProducto[text];
                }
                if (k == 'selMoneda') {
                    text = _moneda[text];
                }
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>', {
                type: 'button'
            }).addClass('btn btn-primary btn-sm seleccionar').append($('<i/>').addClass('glyphicon glyphicon-ok'))).append(' ').appendTo($row);
            $row.appendTo(tbody);
        }
        $tablaProducto.trigger('footable_initialize');
    }

    function refrescarResumen() {
        var totalValorGravado = 0;
        var totalValorDescuentos = 0;
        var totalIGV = 0;
        var totalImporte = 0;
        _.each(_Items, function (e) {
            totalValorGravado += parseFloat(e.txtValorUnitario.replace(/,/g, '')) || 0;
            totalValorDescuentos += parseFloat(e.txtDescuento.replace(/,/g, '')) || 0;
            totalIGV += parseFloat(e.txtIGV.replace(/,/g, '')) || 0;
            totalImporte += parseFloat(e.txtImporte.replace(/,/g, '')) || 0;
        });
        $lblTotalVentaGravado.text(totalValorGravado.toMoney(2));
        $lblTotalValorDescuentos.text(totalValorDescuentos.toMoney(2));
        $lblTotalTotalIGV.text(totalIGV.toMoney(2));
        $lblTotalImporteTotal.text(totalImporte.toMoney(2));
    }

    function renderDireccionesReceptor() {
        var tbody = $tablaDireccionesReceptor.find('tbody');
        tbody.empty();
        for (var i in _DireccionesReceptor) {
            var direccion = _DireccionesReceptor[i];
            var $row = $('<tr/>', {
                'data-row-id': direccion.codigo
            });
            $('<td/>').append($('<button/>').addClass('btn btn-primary agregar-nuevo').append($('<i/>').addClass('glyphicon glyphicon-ok'))).appendTo($row);
            for (var k in _DireccionesReceptorSchema) {
                var text = direccion[k];
                $('<td/>').html(text).appendTo($row);
            }
            $row.appendTo(tbody);
        }
        $tablaDireccionesReceptor.trigger('footable_initialize');
    }

    function validarExistenciaRUC() {
        var formData = {
            txtRUC: $RUCField.val()
        };
        var url = 'emitirbvsimp.do?action=validarExistenciaRucCliente';
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: 'json',
            success: function (data) {
                if (data.estado == '0') {
                    $RUCField.data('pivot-valid', '0');
                    $razonSocial.closest('.form-group').addClass('hidden');
                    $btnValidarDatosReceptor.removeClass('hidden');
                    $btnContinuar.addClass('hidden');
                    alert('El ruc ingresado no existe');
                    return;
                }
                if (data.estado == '2') {
                    $RUCField.data('pivot-valid', '0');
                    $razonSocial.closest('.form-group').addClass('hidden');
                    $btnValidarDatosReceptor.removeClass('hidden');
                    $btnContinuar.addClass('hidden');
                    alert('El ruc ingresado no esta activo');
                    return;
                }
                if (data.estado == '3') {
                    $RUCField.data('pivot-valid', '0');
                    $razonSocial.closest('.form-group').addClass('hidden');
                    $btnValidarDatosReceptor.removeClass('hidden');
                    $btnContinuar.addClass('hidden');
                    alert('El ruc ingresado no esta habido');
                    return;
                }
                if (data.estado == '4') {
                    $RUCField.data('pivot-valid', '0');
                    alert('El ruc ingresado es el mismo del emisor');
                    $razonSocial.closest('.form-group').addClass('hidden');
                    $btnValidarDatosReceptor.removeClass('hidden');
                    $btnContinuar.addClass('hidden');
                    return;
                }
                if (data.estado == '5') {
                    $RUCField.data('pivot-valid', '0');
                    alert('El Usuario no tiene clave de sol');
                    $razonSocial.closest('.form-group').addClass('hidden');
                    $btnValidarDatosReceptor.removeClass('hidden');
                    $btnContinuar.addClass('hidden');
                    return;
                }
                $razonSocial.val(data.razon_social).closest('.form-group').removeClass('hidden');
                $RUCField.data('pivot-valid', '1');
                _DireccionesReceptor = data.items;
                $btnContinuar.removeClass('hidden');
                $btnValidarDatosReceptor.addClass('hidden');
            },
            error: function () {
                $RUCField.data('pivot-valid', '0');
                $razonSocial.closest('.form-group').addClass('hidden');
                $btnValidarDatosReceptor.removeClass('hidden');
                $btnContinuar.addClass('hidden');
            },
            complete: function () {
                $modalPreloader.modal('hide');
                $formBoletaDatosReceptor.valid();
            }
        });
    }

    function validarExistenciaDNI() {
        var formData = {
            txtDocumento: $RUCField.val()
        };
        var url = 'emitirbvsimp.do?action=validarExistenciaDNICliente';
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: 'json',
            success: function (data) {
                if (data.estado == "0") {
                    $RUCField.data('pivot-valid', '0');
                    alert('El dni ingresado no existe');
                    return;
                }
                $razonSocial.val(data.razon_social).closest('.form-group').removeClass('hidden');
                $RUCField.data('pivot-valid', '1');
                $btnContinuar.removeClass('hidden');
                $btnValidarDatosReceptor.addClass('hidden');
            },
            error: function () {
                $RUCField.data('pivot-valid', '0');
                $razonSocial.closest('.form-group').addClass('hidden');
                $btnValidarDatosReceptor.removeClass('hidden');
                $btnContinuar.addClass('hidden');
            },
            complete: function () {
                $modalPreloader.modal('hide');
                $formBoletaDatosReceptor.valid();
            }
        });
    }

    function formatFecha(x) {
        if (!x instanceof Date) throw new Error('not-a-date')
        var day = x.getDate()
        day = day < 10 ? '0' + day : day;
        var month = x.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        var year = x.getFullYear()
        return [day, month, year].join('/');
    }

    function prepararDatosPreview() {
        var d = new Date();
        var retObj = {
            preview: {
                titulo: (function () {
                    return $selTipoDoc.length > 0 ? "Boleta de Venta Electr&oacute;nica" : "Boleta Electr&oacute;nica"
                })()
            },
            emisor: {
                ruc: $hiddenRucEmisor.val(),
                tipoDoc: $selTipoDoc.length == 0 ? "4" : $selTipoDoc.val(),
                direccion: $hiddenDireccionEmisor.val(),
                razon_social: $hiddenRazonSocialEmisor.val(),
                nombre_comercial: $hiddenNombreComercialEmisor.val(),
                departamento: $hiddenDepartamentoEmisor.val()
            },
            boleta: {
                fecha_emision: formatFecha(new Date),
                nombres: $txtRazonSocial.val(),
                ruc: $RUCField.val(),
                numeroDocumento: $RUCField.val(),
                tipoDocumento: $selTipoDoc.val(),
                moneda_iso: $selMoneda.val(),
                direccion_entrega: $txtLugarEntrega.val(),
                total_gravado: $('#total-valor-venta-gravado').text(),
                total_descuentos: $('#total-valor-descuentos').text(),
                total_igv: $('#total-igv').text(),
                total_importe: $('#importe-total').text(),
                txtIndicador: $('#txtIndicador').val()
            },
            documentos_relacionados: _DocumentosRelacionados,
            items: _Items
        };
        var retObj2 = {
            emisor: {
                ruc: $hiddenRucEmisor.val(),
                tipoDoc: $selTipoDoc.length == 0 ? "4" : $selTipoDoc.val(),
                direccion: $hiddenDireccionEmisor.val(),
                razon_social: $hiddenRazonSocialEmisor.val(),
                nombre_comercial: $hiddenNombreComercialEmisor.val(),
                departamento: $hiddenDepartamentoEmisor.val(),
                fecha_vencimiento: $txtFechaVencimiento.val()
            },
            boleta: {
                fecha_emision: formatFecha(new Date),
                nombres: $txtRazonSocial.val(),
                numeroDocumento: $RUCField.val(),
                tipoDocumento: $selTipoDoc.val(),
                moneda_iso: $selMoneda.val(),
                direccion_entrega: $txtLugarEntrega.val(),
                total_gravado: ($('#total-valor-venta-gravado').text()).replace(',', ''),
                total_descuentos: ($('#total-valor-descuentos').text()).replace(',', ''),
                total_igv: ($('#total-igv').text()).replace(',', ''),
                total_importe: ($('#importe-total').text()).replace(',', ''),
                txtIndicador: ($('#txtIndicador').val()).replace(',', '')
            },
            documentos_relacionados: _DocumentosRelacionados,
            items: _Items
        };
        retObj.emisor.tipoDoc_val = _tipoDocumentoIdentidad[retObj.emisor.tipoDoc]
        retObj.documentos_relacionados = _.map(retObj.documentos_relacionados, function (doc) {
            doc.selTipoDocumentoRelacionado_val = _tipoDocumentoRelacionado[doc.selTipoDocumentoRelacionado]
            return doc;
        });
        retObj.items = _.map(retObj.items, function (i) {
            i.selUnidadMedida_val = _tipoMedida[i.selUnidadMedida]
            i.selTipo_val = _tipoProducto[i.selTipo]
            return i;
        });
        retObj.boleta.moneda_simbolo = encodeURIComponent(_moneda[retObj.boleta.moneda_iso]);
        retObj.boleta.moneda = _monedaDescripcion[retObj.boleta.moneda_iso];
        _datosEnvio = retObj2;
        return retObj;
    }

    function showPreview() {
        var data = prepararDatosPreview();
        $('#root-panels').addClass('hidden');
        $("#panel-preview").removeClass('hidden');
        $.get('boleta-xhr.html').success(function (rtext) {
            var tpl = _.template(rtext)
            var parsed = tpl(data)
            $("#preview-boleta").html(parsed).find('table').footable();
        })
    }
    /*
     *  :::: FUNCTION :::: END
     */
    /*
     *  :::: SETUP :::: START
     */
    $tablaDocumento.footable();
    $tablaItem.footable();
    $tablaProducto.footable();
    $tablaDireccionesReceptor.footable();
    $('.modal').on('shown.bs.modal', function () {
        $(this).find('.footable').trigger('footable_initialize');
    })
    $modalDirecciones.on('click', '.agregar', function (e) {
        var direccion = $(this).attr('data-address');
        var indicador = $(this).attr('data-tipo');
        $txtLugarEntrega.val(direccion);
        $txtIndicador.val(indicador);
        $modalDirecciones.modal('hide');
    });
    $modalDirecciones.on('click', '.agregar-nuevo', function (e) {
        _direccionSeleccionada = $(this).closest('tr').attr('data-row-id');
        var direccion = _.find(_DireccionesReceptor, {
            codigo: _direccionSeleccionada
        })
        $txtLugarEntrega.val(direccion.domicilio);
        $modalDirecciones.modal('hide');
    });
    $tablaDocumento.on('click', 'button.remover', function (e) {
        var rowId = $(e.target).closest('tr').attr('data-row-id');
        eliminarDocumentoRelacionado({
            rowId: rowId
        });
    })
    $tablaItem.on('click', 'button.remover', function (e) {
        var rowId = $(e.target).closest('tr').attr('data-item-id');
        eliminarItem({
            itemId: rowId
        });
        refrescarResumen();
    })
    $tablaItem.on('click', 'button.editar', function (e) {
        var rowId = $(e.target).closest('tr').attr('data-item-id');
        var currItem = buscarItem({
            itemId: rowId
        });
        _itemEditando = currItem.itemId;
        popularModalItem(currItem);
        $modalItem.modal('show');
    })
    $btnOpenModalDR.on('click', function () {
        $formModalDocumentoRelacionado.find('.alert').remove();
        $formModalDocumentoRelacionado.find('.has-error').removeClass('has-error');
        $formModalDocumentoRelacionado.find(':input').val('');
        $modalDocumentoRelacionado.modal('show');
    })
    $btnOpenModalItem.on('click', function () {
        if (_Items.length >= 10) {
            alert('Se permite un limite de 10 items');
            return;
        }
        _itemEditando = false;
        $formModalItem.find('.alert').remove();
        $formModalItem.find('.has-error').removeClass('has-error');
        $formModalItem.find(':input').val('');
        //$formModalItem.find('#txtCantidad').val((1).toMoney(9, ''));
        $formModalItem.find('#txtCantidad').val((1).toMoney(2, ''));
        $formModalItem.find('#txtDescuento').val('0.00');
        $modalItem.modal('show');
    });
    $btnOpenModalSearch.on('click', function () {
        renderProductos();
        $modalSearch.modal('show');
    });
    $txtProductQuery.on('keyup', function (e) {
        var query = $txtProductQuery.val();
        var productos = filtarProductos(query);
        renderProductos(productos);
    });
    $tablaProducto.on('click', 'button.seleccionar', function (e) {
        var pid = $(e.target).closest('tr').attr('data-p-id');
        var producto = _.find(_Productos, {
            pId: pid
        });
        $formModalItem.find('#txtDescripcion').val(producto.txtDescripcion);
        $modalSearch.modal('hide');
    })

    $('[data-format-numeric]').on('change', function (e) {
        var $self = $(this);
        var places = $self.attr('data-decimal-places') || 2;
        var groupDelimiter = $self.attr('data-group-delimiter') || undefined;
        console.log($(this).numVal())
        e.target.value = $(this).numVal().toMoney(places, groupDelimiter);
    });

    $('.calculate-total').bind('change keyup', function (e) {
        var cantidad = $formModalItem.find('#txtCantidad').numVal();
        var valorUnidad = $formModalItem.find('#txtValorUnitario').numVal();
        var descuento = $formModalItem.find('#txtDescuento').numVal();

        if (!( _.isNumber(cantidad) && _.isNumber(valorUnidad) && _.isNumber(descuento) ))
        	return;

        var $igv = $formModalItem.find('#txtIGV');
        var $importe = $formModalItem.find('#txtImporte');
        var subtotal = (cantidad * valorUnidad) - descuento;
        var impuesto = 0.18 * subtotal;
        var total = impuesto + subtotal;
        //$igv.val(impuesto.toMoney(2));
        $igv.val(impuesto.toFixed(2));
        //$importe.val(total.toMoney(2));
        $importe.val(total.toFixed(2));
    });
    $('.calculate-from-importe').on('keyup', function (argument) {
        var $igv = $formModalItem.find('#txtIGV');
        var $importe = $formModalItem.find('#txtImporte');
        var $valorUnitario = $formModalItem.find('#txtValorUnitario');
        var cantidad = $formModalItem.find('#txtCantidad').numVal();
        var importe = $formModalItem.find('#txtImporte').numVal();
        var descuento = $formModalItem.find('#txtDescuento').numVal();
        var valorUnidad = ((importe / 1.18) + descuento) / cantidad;
        var subtotal = (cantidad * valorUnidad) - descuento;
        var impuesto = 0.18 * subtotal;
        var total = impuesto + subtotal;
        $valorUnitario.val(valorUnidad.toMoney(2));
        $igv.val(impuesto.toMoney(2));
    })
    $txtCodigo.add($txtDescripcion).on('typeahead:select', function (e, s) {
        var producto = _.find(_Productos, {
            txtCodigo: s.txtCodigo
        });
        if (producto) popularModalItem(producto, true, false);
    });
    $("#selTipo").on('change', function (e) {
        if (e.target.value == 'TI02') {
            $('#txtCantidad').val(1).attr('readonly', 'readonly');
        } else {
            $('#txtCantidad').removeAttr('readonly');
        }
    })
    var _CURR_DATE_PICKER = null;
    $('.datepicker').on('click', function (e) {
        _CURR_DATE_PICKER = $(e.target);
        $modalDatepicker.modal('show');
    })
    $datepicker.datepicker({
        // endDate: new Date(),
        startDate: new Date()
    }).on('changeDate', function (e) {
        $datepickerHidden.val(e.format());
    });
    $btnCloseDatepicker.on('click', function (e) {
        var selectedDate = $datepickerHidden.val();
        _CURR_DATE_PICKER.val(selectedDate).trigger('change');
        $datepickerHidden.val('');
        _CURR_DATE_PICKER = null;
        $modalDatepicker.modal('hide');
    });
    _.each(_tipoMedida, function (v, k) {
        $('<option/>', {
            value: k
        }).html(v).appendTo($selUnidadMedida);
    })
    _.each(_tipoProducto, function (v, k) {
        $('<option/>', {
            value: k,
            selected: (k == 'TI01')
        }).html(v).appendTo($selTipo);
    })
    _.each(_monedaDescripcion, function (v, k) {
        $('<option/>', {
            value: k,
            selected: (k == 'PEN')
        }).html(v).appendTo($selMoneda);
    })
    _.each(_tipoDocumentoRelacionado, function (v, k) {
        $('<option/>', {
            value: k
        }).html(v).appendTo($selTipoDocumentoRelacionado);
    })
    var ProductoCodigoBH = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('txtCodigo', 'txtDescripcion'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: [],
        identify: function (obj) {
            return obj.txtCodigo;
        },
    });
    $txtCodigo.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'codigos',
        source: ProductoCodigoBH,
        templates: {
            suggestion: function (e) {
                var par = $('<div/>')
                $('<em/>').text(e.txtCodigo).appendTo(par);
                par.append(' - ');
                $('<span/>').text(e.txtDescripcion).appendTo(par);
                return par;
            }
        },
        display: 'txtCodigo'
    });
    $txtDescripcion.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'codigos',
        source: ProductoCodigoBH,
        templates: {
            suggestion: function (e) {
                var par = $('<div/>')
                $('<em/>').text(e.txtCodigo).appendTo(par);
                par.append(' - ');
                $('<span/>').text(e.txtDescripcion).appendTo(par);
                return par;
            }
        },
        display: 'txtDescripcion'
    });
    $('.tt-query').css('background-color', '#fff');
    $RUCField.on('change', function () {
        $btnContinuar.addClass('hidden');
        $btnValidarDatosReceptor.removeClass('hidden');
        $RUCField.data('pivot-valid', '1');
        if ($selTipoDoc.length > 0) {
            if ($selTipoDoc.val() == '1') $formBoletaDatosReceptor.valid();
            else $formBoletaDatosReceptor.valid();
        }
    });
    $btnContinuar.on('click', function () {
        $btnContinuar.addClass('hidden');
        $RUCField.attr('disabled', 'disabled')
        $razonSocial.attr('disabled', 'disabled')
        $selTipoDoc.attr('disabled', 'disabled')
        $RUCField.attr('disabled', 'disabled')
        $panelBoletaDatosReceptor.find('.panel-collapse').collapse('hide');
        $panelDatosComprobante.removeClass('hidden').find('.panel-collapse').collapse('show');
        $panelDatosComprobante.removeClass('hidden').find('.panel-collapse').collapse('show');
        $panelDocumentosRelacionados.removeClass('hidden').find('.panel-collapse').collapse('show');
        $panelItems.removeClass('hidden').find('.panel-collapse').collapse('show');
        $panelResumen.removeClass('hidden').find('.panel-collapse').collapse('show');
        $("#buttons").removeClass('hidden');
        renderDireccionesReceptor();
        prefetchProductos();
        renderDocumentoRelacionado()
        renderItem();
        refrescarResumen();
    });
    $selMoneda.on('focus', function (e) {
        $(this).attr('data-previous', this.value)
    });
    $selMoneda.on('change', function (evt) {
        var prev = $(this).attr('data-previous')
        
        if(_Items.length > 0 && this.value != prev){
            alert('remover-items-existentes');
            this.value = prev;
            return;
        }
      $(".simbolo-moneda").html( _moneda[this.value] )
      ProductoCodigoBH.clear();
        var filtered = _.filter(_Productos, function(e){
            return _.has(e.selMoneda, evt.target.value)
        })
      ProductoCodigoBH.add(filtered);
    })
    $btnMostrarModalItems.on('click', function (e) {
        if (!$selMoneda.val()) {
            alert('Seleccione la Moneda');
            return;
        }
        $modalListaItems.modal('show');
    });
    $btnMostrarModalDR.on('click', function (e) {
        $modalListaDocumentoRelacionado.modal('show');
    });
    $btnPreview.on('click', function (e) {
        if (!$formComprobante.valid()) return;
        if (!$formItem.valid()) return;
        showPreview();
    })
    $btnVolverBoleta.on('click', function (e) {
        $('#root-panels').removeClass('hidden')
        $("#panel-preview").addClass('hidden');
    })
    $btnEmitirBoleta.on('click', function (e) {
        $btnEmitirBoleta.prop('disabled', true);
        /// quickfix 15-10-2015
        if (!confirm('\u00BFEsta seguro de emitir su comprobante?')) {
            return;
        }
        var url = "emitirbvsimp.do?action=grabarComprobante";
        $.ajax({
            data: {
                variable: JSON.stringify(_datosEnvio),
            },
            method: 'post',
            //contentType: "application/text; charset=UTF-8",
            dataType: 'json',
            url: url,
        }).done(function (data, textStatus, jqXHR) {
            if (console && console.log) {
                console.log("La solicitud se ha completado correctamente.");
                window.location.href = "emitirbvsimp.do?action=mostrarBoletaGenerada";
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    })
    $selTipoDocumentoRelacionado.on('change', function (e) {
        $txtSerie.parent().toggleClass('hidden', e.target.value == '00')
    })

    $selTipoDoc.on('change', function (e) {

        $razonSocial.parent().toggleClass('hidden', this.value == '1' || this.value == '4')

        $RUCField.attr('maxlength', this.value == '1' ? 8 : 15)     
        $RUCField.attr('maxlength', this.value == '4' ? 11 : 15)     
        $RUCField.val('');
        if(e.target.value == '1' || e.target.value == '4')
            $txtRazonSocial.attr('readonly', 'readonly')
        else
            $txtRazonSocial.removeAttr('readonly')

        if ($selTipoDoc.val() == '7'){
            $RUCField.val('-')
            $RUCField.attr('readonly', 'readonly')
        }else{
            $RUCField.removeAttr('readonly')
            $RUCField.val('')
        }
    })

        /*
         *  :::: SETUP :::: END
         */
        /*
         *  :::: Form Validate :::: START
         */
    $formCatalogo.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            txtCodigo: {
                required: true,
                minlength: 11
            },
            txtDescripcion: {
                required: true
            }
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            txtCodigo: "Ingrese el codigo",
            txtDescripcion: "Ingrese la descripcion"
        },
        submitHandler: function (form) {
            form.submit();
            /*
            var url = '';
            $.ajax({
              method: 'post',
              dataType: 'json',
              complete: function () {
                window.location.href = "tabla-resultado.html"
              }
            })

            */
        }
    }));


/*
  $.validator.addMethod("equals", function(value, element, string) {
    return $.inArray(value, string) !== -1;
}, $.validator.format("Please enter '{0}'"));
*/
    $formModalDocumentoRelacionado.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            selTipoDocumentoRelacionado: {
                required: true,
            },
            txtSerie: {
                required: function () {
                    return ['09','31'].indexOf($selTipoDocumentoRelacionado.val()) >= 0;
                },
                number: function(control) {
                    return ($selTipoDocumentoRelacionado.val() == '09' && control.value != 'EG01') ? true : undefined; 
                },
                regex: function () {
                    if($selTipoDocumentoRelacionado.val() == '31')
                        return /^T[a-z0-9]{1,4}$/ig;
                }
            },
            txtNumero: {
                required: true,
                number: true,
                maxlength: 10
            },
            txtDescripcion: {
                required: false,
            },
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            selTipoDocumentoRelacionado: {
                required: 'Seleccione el tipo de documento'
            },
            txtSerie: {
                required: 'Ingrese la Serie',
                number: 'Verifique la serie // EG01',
                regex: 'ejemplo de serie TXXXX'
            },
            txtNumero: {
                required: 'Ingrese el n&uacute;mero',
                number: 'Verifique el n&uacute;mero',
                maxlength: 'Verifique el n&uacute;mero'
            },
            txtDescripcion: {
                required: 'Ingrese la Descripci&oacute;n'
            },
        },
        submitHandler: function (form) {
            /// START eliminacion de duplicados //////
            var data = $(form).first().serializeObject();
            var criteria = {
                selTipoDocumentoRelacionado: data.selTipoDocumentoRelacionado,
                txtNumero: data.txtNumero
            };
            if (data.selTipoDocumentoRelacionado == '00') {
                criteria.txtSerie = data.txtSerie;
            }
            duplicated = !!_.findWhere(_DocumentosRelacionados, criteria);
            if (duplicated) {
                alert('El registro ya ha sido ingresado');
                return;
            }
            /// END eliminacion de duplicados //////
            data.rowId = _.uniqueId('dr-');
            agregarDocumentoRelacionado(data);
            $modalDocumentoRelacionado.modal('hide');
        }
    }))
    $formModalItem.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            txtCantidad: {
                required: true,
                moreThan: 0
            },
            selUnidadMedida: {
                required: true,
            },
            txtCodigo: {
                required: true,
                maxlength: 15
            },
            txtDescripcion: {
                required: true,
                maxlength: 200,
            },
            txtValorUnitario: {
                required: true,
                moreThan: 0
            },
            txtImporte: {
                moreThan: 0
            },
            txtIGV: {
                moreThan: 0
            },
            txtDescuento: {
                required: false
            },
            selTipo: {
                required: true,
            }
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            txtCantidad: {
                required: 'Ingrese la Cantidad',
                moreThan: 'La cantidad debe ser mayor a 0'
            },
            selUnidadMedida: {
                required: 'Seleccione Unidad de Medida',
            },
            txtCodigo: {
                required: 'Ingrese el C&oacute;digo',
            },
            txtDescripcion: {
                required: 'Ingrese la Descripci&oacute;n',
            },
            txtValorUnitario: {
                required: 'Ingrese el Valor Unitario',
                moreThan: 'El valor del precio unitario no es  v&aacute;lido'
            },
            txtDescuento: {
                required: 'El descuento es requerido',
                moreThan: 'El valor del descuento no es  v&aacute;lido'
            },
            txtIGV: {
                required: 'El Igv es requerido',
                moreThan: 'El valor del igv no es  v&aacute;lido'
            },
            txtImporte: {
                required: 'El Importe es requerido',
                moreThan: 'El valor del igv no es  v&aacute;lido'
            },
            selTipo: {
                required: 'Seleccione el Tipo de Servicio',
            }
        },
        submitHandler: function (form) {
            var data = $(form).last().serializeObject();
            if (_itemEditando) {
                editarItem({
                    itemId: _itemEditando
                }, data);
            } else {
                data.itemId = _.uniqueId('');
                agregarItem(data);
            }
            $modalItem.modal('hide');
            _itemEditando = false;
            refrescarResumen();
        }
    }));
    $formBoletaDatosReceptor.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        /*
        onkeyup: false,
        onchange: false,
        onclick: false,
        onfocusout: false,
        */
        rules: {
            txtDocumento: {
                required: function () {
                    return $selTipoDoc.val() != '7'
                },
                minlength: function () {
                    if ($selTipoDoc.val() == '7')
                        return false
                    if ($selTipoDoc.val() == '1') return 8;
                    else if ($selTipoDoc.val() == '4') return 11;
                    else return 15;
                },
                maxlength: function () {
                    if ($selTipoDoc.val() == '1') return 8;
                    else if ($selTipoDoc.val() == '4') return 11;
                    else return 15;
                },
                number: function () {
                    if ($selTipoDoc.val() == '7')
                        return false
                    if ($selTipoDoc.val() == '1' || $selTipoDoc.val() == '4') return true;
                },
            },
            txtRazonSocial: {
                required: function () {
                    return ['1', '4', '7'].indexOf($selTipoDoc.val()) === -1;
                }
            }
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {},
        submitHandler: function (form) {
            if(!$razonSocial.val())
                $razonSocial.val('-')
            if ($selTipoDoc.val() == '1') {
                $modalPreloader.modal('show');
                validarExistenciaDNI();
            } else if ($selTipoDoc.val() == '4') {
                $modalPreloader.modal('show');
                validarExistenciaRUC();
            } else {
                $btnContinuar.removeClass('hidden');
                $btnValidarDatosReceptor.addClass('hidden');
            }
        }
    }));
    $formItem.validate(_.extend(window._validatorWallSettings, {
        rules: {
            hiddenHelperItem: {
                moreThan: 0,
                lessThan: 10
            }
        },
        ignore: '.ignored-field, .tt-hint',
        messages: {
            hiddenHelperItem: {
                moreThan: function () {
                    if ($selTipoDoc.length == 0) return "Agregue un item a la boleta"
                    else return "Agregue un item a la boleta"
                },
                lessThan: function () {
                    if ($selTipoDoc.length == 0) return "Solo se permiten 10 items en la boleta."
                    else return "Solo se permiten 10 items en la boleta."
                }
            }
        }
    }))
    $formComprobante.validate(_.extend(window._validatorWallSettings, {
        rules: {
            selMoneda: {
                required: true
            },
            txtLugarEntrega: {
                required: function (argument) {
                    return $selTipoDoc.length == 0;
                }
            }
        },
        messages: {
            selMoneda: {
                required: "Seleccione la Moneda"
            },
            txtLugarEntrega: {
                required: "Ingrese el Lugar de entrega"
            }
        }
    }));

    function soloNumeros(e) {
        var key = window.Event ? e.which : e.keyCode
        return (key >= 48 && key <= 57)
    }

    function soloLetrasNumerosEspacios(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";
        especiales = "8-37-39-46";
        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function SoloNumeroDecimal(e, field) {
        key = e.keyCode ? e.keyCode : e.which
            // backspace
        if (key == 8) return true
            // 0-9 a partir del .decimal  
        if (field.value != "") {
            if ((field.value.indexOf(".")) > 0) {
                //si tiene un punto valida dos digitos en la parte decimal
                if (key > 47 && key < 58) {
                    if (field.value == "") return true
                        //regexp = /[0-9]{1,10}[\.][0-9]{1,3}$/
                    regexp = /[0-9]{2}$/
                    return !(regexp.test(field.value))
                }
            }
        }
        // 0-9 
        if (key > 47 && key < 58) {
            if (field.value == "") return true
            regexp = /[0-9]{10}/
            return !(regexp.test(field.value))
        }
        // .
        if (key == 46) {
            if (field.value == "") return false
            regexp = /^[0-9]+$/
            return regexp.test(field.value)
        }
        // other key
        return false
    }
    window.onload = function () {
        // $selTipoDoc.val('7').trigger('change');
        // $RUCField.val('adasdasdasdasda').trigger('change');
        // $razonSocial.val('asdasdasdasdasd').trigger('change');
        // $formBoletaDatosReceptor.trigger('submit');
    }
        
        /*
         *  :::: Form Validate :::: END
         */

// })(window, jQuery, _);