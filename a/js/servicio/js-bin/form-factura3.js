// (function(window, $, _) {
// avance al 08-09-2015 @ 22:48 VET
    /*
     * :::: CONTAINERS :::: START
     */
    var $formCatalogo = $("#form-catalogo");
    var $txtRazonSocial = $("#txtRazonSocial");
    var $txtLugarEntrega = $("#txtLugarEntrega");
    
    var $modalDirecciones = $("#modal-direcciones");
    var $tablaDireccionesReceptor = $modalDirecciones.find("#tabla-direcciones-receptor");

    var $tablaDocumento = $("#tabla-documento-relacionado");
    var $tablaItem = $("#tabla-item");
    var $modalListaDocumentoRelacionado = $("#modal-mostrar-documento-relacionado")
    var $modalListaItems = $("#modal-mostrar-items")
    var $modalDocumentoRelacionado = $("#modal-documento-relacionado")
    var $formModalDocumentoRelacionado = $modalDocumentoRelacionado.find('#form-modal-documento-relacionado');
    var $modalItem = $("#modal-item")
    var $formModalItem = $modalItem.find('#form-modal-item');
    var $txtCodigo = $formModalItem.find('#txtCodigo');
    var $txtDescripcion = $formModalItem.find('#txtDescripcion');
    var $selUnidadMedida = $formModalItem.find('#selUnidadMedida');
    var $selTipo = $formModalItem.find('#selTipo');

    var $btnOpenModalDR = $('#open-modal-documento-relacionado');
    var $btnOpenModalItem = $('#open-modal-item');
    var $btnOpenModalSearch = $('#open-modal-search');
    var $modalSearch = $("#modal-search");
    var $tablaProducto = $modalSearch.find("#tabla-producto");
    var $formSearch = $modalSearch.find('#form-modal-search');
    var $txtProductQuery = $formSearch.find('#product-query');
    var $lblTotalVentaGrabado = $('#total-valor-venta-grabado');
    var $lblTotalValorDescuentos = $('#total-valor-descuentos');
    var $lblTotalTotalIGV = $('#total-igv');
    var $lblTotalImporteTotal = $('#importe-total');
    var $modalDatepicker = $('#modal-datepicker');
    var $datepicker = $modalDatepicker.find("#datepicker-placeholder");
    var $datepickerHidden = $modalDatepicker.find("#datepicker-value");
    var $btnCloseDatepicker = $modalDatepicker.find('#modal-datepicker-close');
    var $formDatosReceptor = $('#form-datos-receptor');
    var $RUCField = $formDatosReceptor.find('#txtRuc');
    var $razonSocial = $formDatosReceptor.find('#txtRazonSocial');
    var $btnValidarDatosReceptor = $formDatosReceptor.find('#btnValidarDatosReceptor');
    var $btnContinuar = $formDatosReceptor.find('#btnContinuarPaso');
    var $modalPreloader = $('#modalPreloader');
    
    var $panelDatosReceptor = $('#panel-datos-receptor');
    var $panelDatosComprobante = $('#panel-datos-comprobante');
    var $panelDocumentosRelacionados = $('#panel-documentos-relacionados');
    var $panelItems = $('#panel-items');
    var $panelResumen = $('#panel-resumen');

    var $btnMostrarModalDR =  $panelDocumentosRelacionados.find('#btnMostrarModalDR');
    var $btnMostrarModalItems = $panelItems.find('#btnMostrarModalItems');
    var $selMoneda = $panelDatosComprobante.find('#selMoneda');

    var $btnPreview = $('#btnPreview');
    var $btnVolverFactura = $('#btnVolverFactura');

    var $hiddenRucEmisor = $formDatosReceptor.find("#rucEmisor");
    var $hiddenDireccionEmisor = $formDatosReceptor.find("#direccionEmisor");
    var $hiddenRazonSocialEmisor = $formDatosReceptor.find("#razonSocialEmisor");

    var $formItem = $("#form-item")
    var $hiddenHelperItem = $formItem.find("#hiddenHelperItem");
    var $formDR = $("#form-modal-documento-relacionado")
    var $hiddenHelperDR = $formDR.find("#hiddenHelperDR");
    var $formComprobante = $("#form-datos-comprobante")

    /* 
     *  :::: CONTAINERS :::: END
     */
    /*
     *  :::: HELPERS :::: START
     */
    var _tipoDocumentoRelacionado = {
        '001': 'Guia de Remisi&oacute;n',
        '002': 'Guia de Remisi&oacute;n de Transporte',
    }
    var _tipoMedida = {
        '4A':'BOBINAS',
'BE':'FARDO',
'BG':'BOLSA',
'BJ':'BALDE',
'BLL':'BARRILES',
'BO':'BOTELLAS',
'BX':'CAJA',
'C62':'PIEZAS',
'CA':'LATAS',
'CEN':'CIENTO DE UNIDADES',
'CJ':'CONOS',
'CMK':'CENTIMETRO CUADRADO',
'CMQ':'CENTIMETRO CUBICO',
'CMT':'CENTIMETRO LINEAL',
'CT':'CARTONES',
'CY':'CILINDRO',
'DR':'TAMBOR',
'DZN':'DOCENA',
'DZP':'DOCENA POR 0**6 ',
'FOT':'PIES',
'FTK':'PIES CUADRADOS',
'FTQ':'PIES CUBICOS',
'GLI':'GALON INGLES (4,545956L)',
'GLL':'US GALON (3,7843 L)',
'GRM':'GRAMO',
'GRO':'GRUESA',
'HLT':'HECTOLITRO',
'INH':'PULGADAS',
'KGM':'KILOGRAMO',
'KT':'KIT',
'KTM':'KILOMETRO',
'KWH':'KILOVATIO HORA',
'LBR':'LIBRAS',
'LEF':'HOJA',
'LTN':'TONELADA LARGA',
'LTR':'LITRO',
'MGM':'MILIGRAMOS',
'MLL':'MILLARES',
'MLT':'MILILITRO',
'MMK':'MILIMETRO CUADRADO',
'MMQ':'MILIMETRO CUBICO',
'MMT':'MILIMETRO '
'MTK':'METRO CUADRADO',
'MTQ':'METRO CUBICO',
'MTR':'METRO',
'MWH':'MEGAWATT HORA',
'NIU':'UNIDAD',
'ONZ':'ONZAS',
'PF':'PALETAS',
'PG':'PLACAS ',
'PK':'PAQUETE',
'PR':'PAR',
'RM':'RESMA',
'SET':'JUEGO',
'ST':'PLIEGO',
'STN':'TONELADA CORTA',
'TNE':'TONELADAS',
'TU':'TUBOS',
'UM':'MILLON DE UNIDADES',
'YDK':'YARDA CUADRADA',
'YRD':'YARDA',
'ZZ':'UNIDAD'
    }

    var _moneda = {
        'USD': '&dollar;',
        'PEN': 'S/.',
        'EUR': '&euro;',
        'GBP': '&pound;',
        'JPY': '&yen;',
        'SEC': 'kr',
        'CHF': 'CHF'
    }
    var _monedaDescripcion = {
        'PEN': 'NUEVOS SOLES',
        'EUR': 'EUROS',
        'USD': 'DOLAR AMERICANO',
        'GBP': 'LIBRA ESTERLINA',
        'JPY': 'YEN',
        'SEC': 'CORONA SUECA',
        'CHF': 'FRANCO SUIZO'
    }

    var _tipoProducto = {
        'bien': 'Bien',
        'servicio': 'Servicio'
    }
    var _itemEditando = false;
    var _DocumentosRelacionadosSchema = {
        selTipoDocumento: "Tipo de Documento",
        txtSerie: "Serie",
        txtNumero: "N&uacute;mero",
        txtDescripcion: "Descripci&oacute;n"
    }
    var _DocumentosRelacionados = [];
    var _Items = [];
    var _DireccionesReceptor = [];
    var _DireccionesReceptorSchema = {
        "tipo": "Tipo de Domicilio",
        "domicilio": "Domicilio",
    };
    var _direccionSeleccionada = -1;
    var _itemSchema = {
        txtCantidad: "Cantidad",
        selTipo: "Tipo",
        selUnidadMedida: "Unidad de Medida",
        txtCodigo: "C&oacute;digo",
        txtDescripcion: "Descripci&oacute;n",
        txtValorUnitario: "Valor Unitario",
        txtDescuento: "Descuento",
        txtIGV: "IGV",
        txtImporte: "Importe",
    }
    var _Productos = [];
    var _ProductoSchema = {
            txtCodigo: "C&oacute;digo",
            txtDescripcion: "Descripci&oacute;n",
            selTipo: "Tipo",
            selUnidadMedida: "Unidad de Medida",
            selMoneda: "Moneda",
            txtValorUnitario: "Precio de Venta",
        }
        /*
         *  :::: HELPERS :::: END
         */
        /*
         *  :::: FUNCTION :::: START
         */
    function renderDocumentoRelacionado() {
        var tbody = $tablaDocumento.find('tbody');
        tbody.empty();
        for (var i in _DocumentosRelacionados) {
            var doc = _DocumentosRelacionados[i];
            var $row = $('<tr/>', {
                'data-row-id': doc.rowId
            });
            for (var k in _DocumentosRelacionadosSchema) {
                var text = doc[k];
                if (k == 'selTipoDocumento') {
                    text = _tipoDocumentoRelacionado[text];
                }
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>').addClass('btn btn-danger btn-sm remover').append($('<i/>').addClass('glyphicon glyphicon-remove'))).appendTo($row);
            $row.appendTo(tbody);
        }
        $("#documentos-relacionados-counter").text(_DocumentosRelacionados.length)
        $hiddenHelperDR.val(_DocumentosRelacionados.length);
        $tablaDocumento.trigger('footable_initialize');
    }

    function agregarDocumentoRelacionado(data) {
        _DocumentosRelacionados.push(data);
        renderDocumentoRelacionado();
    }

    function eliminarDocumentoRelacionado(criteria) {
        _DocumentosRelacionados = _.reject(_DocumentosRelacionados, criteria)
        renderDocumentoRelacionado();
    }

    function editarDocumentoRelacionado(criteria, data) {
        var doc = _.find(_DocumentosRelacionados, criteria)
        doc = _.extend(doc, data);
        renderDocumentoRelacionado();
    }

    function renderItem() {
        var tbody = $tablaItem.find('tbody');
        tbody.empty();
        for (var i in _Items) {
            var doc = _Items[i];
            var $row = $('<tr/>', {
                'data-item-id': doc.itemId
            });
            for (var k in _itemSchema) {
                var text = doc[k];
                if (k == 'selUnidadMedida') {
                    text = _tipoMedida[text];
                }
                if (k == 'selTipo') continue;
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>').addClass('btn btn-danger btn-sm remover').append($('<i/>').addClass('glyphicon glyphicon-remove'))).appendTo($row);
            $row.appendTo(tbody);
        }
        $("#items-counter").text(_Items.length)
        $hiddenHelperItem.val(_Items.length);
        $tablaItem.trigger('footable_initialize');
    }

    function agregarItem(data) {
        _Items.push(data);
        renderItem();
    }

    function eliminarItem(criteria) {
        _Items = _.reject(_Items, criteria)
        renderItem();
    }

    function buscarItem(criteria) {
        return _.find(_Items, criteria);
    }

    function editarItem(criteria, data) {
        var doc = buscarItem(criteria);
        doc = _.extend(doc, data);
        renderItem();
    }

    function popularModalItem(data, triggerChange, ignoreCodigo) {
        for (var i in _itemSchema) {
            if (!data.hasOwnProperty(i) || ignoreCodigo && i == 'txtCodigo') continue;
            var el = $formModalItem.find('#' + i);
            el.val(data[i]);
            if (triggerChange) el.trigger('keyup').trigger('change');
        }
        if(data.hasOwnProperty('selMoneda')){
            var totales = data.selMoneda;
            var divisa = $selMoneda.val()
            if(totales.hasOwnProperty(divisa)){
              $formModalItem.find('#txtValorUnitario').val(totales[divisa]);
              if (triggerChange)
                $formModalItem.find('#txtValorUnitario').trigger('keyup').trigger('change');
            }
        }
    }

    function prefetchProductos() {
        $.getJSON('a/js/json/catalogo.json', function(data) {
            _Productos = data;
            ProductoCodigoBH.add(data);
        })
    }

    function filtarProductos(descripcion) {
        var regex = new RegExp(descripcion, "ig");
        return _.filter(_Productos, function(curr) {
            return curr.txtDescripcion.match(regex) !== null;
        });
    }

    function renderProductos(productos) {
        productos = productos || _Productos;
        var tbody = $tablaProducto.find('tbody');
        tbody.empty();
        for (var i in productos) {
            var doc = productos[i];
            var $row = $('<tr/>', {
                'data-p-id': doc.pId
            });
            for (var k in _ProductoSchema) {
                var text = doc[k];
                if (k == 'selUnidadMedida') {
                    text = _tipoMedida[text];
                }
                if (k == 'selTipo') {
                    text = _tipoProducto[text];
                }
                if (k == 'selMoneda') {
                    text = _moneda[text];
                }
                $('<td/>').html(text).appendTo($row);
            }
            $('<td/>').append($('<button/>', {
                type: 'button'
            }).addClass('btn btn-primary btn-sm seleccionar').append($('<i/>').addClass('glyphicon glyphicon-ok'))).append(' ').appendTo($row);
            $row.appendTo(tbody);
        }
        $tablaProducto.trigger('footable_initialize');
    }

    function refrescarResumen() {
        var totalValorGravado = 0;
        var totalValorDescuentos = 0;
        var totalIGV = 0;
        var totalImporte = 0;
        _.each(_Items, function(e) {
            totalValorGravado += parseFloat(e.txtValorUnitario) || 0;
            totalValorDescuentos += parseFloat(e.txtDescuento) || 0;
            totalIGV += parseFloat(e.txtIGV) || 0;
            totalImporte += parseFloat(e.txtImporte) || 0;
        });
        $lblTotalVentaGrabado.text(totalValorGravado.toMoney(2));
        $lblTotalValorDescuentos.text(totalValorDescuentos.toMoney(2));
        $lblTotalTotalIGV.text(totalIGV.toMoney(2));
        $lblTotalImporteTotal.text(totalImporte.toMoney(2));
    }

    function renderDireccionesReceptor() {
        var tbody = $tablaDireccionesReceptor.find('tbody');
        tbody.empty();
        for (var i in _DireccionesReceptor) {
            var direccion = _DireccionesReceptor[i];
            var $row = $('<tr/>', {
                'data-row-id': direccion.codigo
            });
            $('<td/>').append(
              $('<button/>').addClass('btn btn-primary agregar-nuevo').append(
                $('<i/>').addClass('glyphicon glyphicon-ok')
              )
            ).appendTo($row);
            for (var k in _DireccionesReceptorSchema) {
                var text = direccion[k];
                $('<td/>').html(text).appendTo($row);
            }
            $row.appendTo(tbody);
        }
        $tablaDireccionesReceptor.trigger('footable_initialize');
    }

    function validarExistenciaRUC(){
      var formData = {
        txtRUC: $RUCField.val()
      };
      // var url = 'emisionrhe.do?action=validarDocumento'; 
      var url = ['a/js/json/cliente.json'].join('');
      $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: 'json',
        success: function(data) {
          if(data.estado == '0'){ 
            $RUCField.data('pivot-valid', '0');
            return; 
          }
          if(data.estado == '2'){ 
          }
          if(data.estado == '3'){ 
          }

          $razonSocial
            .val(data.razon_social)
            .closest('.form-group')
            .removeClass('hidden');
          $RUCField.data('pivot-valid', '1');
          _DireccionesReceptor = data.items;
          $btnContinuar.removeClass('hidden');
          $btnValidarDatosReceptor.addClass('hidden');
        },
        error: function () {
          $RUCField.data('pivot-valid', '0');
          $razonSocial
            .closest('.form-group')
            .addClass('hidden');
          $btnValidarDatosReceptor.removeClass('hidden');
          $btnContinuar.addClass('hidden');
        },
        complete: function () {
          $modalPreloader.modal('hide');
          $formDatosReceptor.valid();
        }
      });
    }

    function formatFecha (x) {
      if (!x instanceof Date)
        throw new Error('not-a-date')
      var day = x.getDate()
      day = day < 10 ? '0'+day : day;
      var month = x.getMonth()+1;
      month = month < 10 ? '0'+month : month;
      var year = x.getFullYear()
      return [day, month, year].join('/');
    }

    function prepararDatosPreview () {
      var d = new Date();
      var retObj = {
        emisor: {
          ruc: $hiddenRucEmisor.val(),
          direccion: $hiddenDireccionEmisor.val(),
          razon_social: $hiddenRazonSocialEmisor.val()
        },
        factura: {
            fecha_emision: formatFecha(new Date),
            razon_social: $txtRazonSocial.val(),
            ruc: $RUCField.val(),
            moneda: $selMoneda.val(),
            direccion_entrega: $txtLugarEntrega.val(),
            total_gravado: $('#total-valor-venta-grabado').text(),
            total_descuentos: $('#total-valor-descuentos').text(),
            total_igv: $('#total-igv').text(),
            total_importe: $('#importe-total').text()
        },
        documentos_relacionados: _DocumentosRelacionados,
        items: _Items
      };

      retObj.documentos_relacionados = _.map(retObj.documentos_relacionados, function (doc) {
        doc.selTipoDocumento = _tipoDocumentoRelacionado[doc.selTipoDocumento]
        return doc;
      });

      retObj.items = _.map(retObj.items, function (i) {
        i.selUnidadMedida = _tipoMedida[i.selUnidadMedida]
        i.selTipo = _tipoProducto[i.selTipo]
        return i;
      });
      retObj.factura.moneda_simbolo = _moneda[retObj.factura.moneda];
      retObj.factura.moneda = _monedaDescripcion[retObj.factura.moneda];
      return retObj;
    }

    function showPreview () {
        var data = prepararDatosPreview();
        $('#root-panels').addClass('hidden');
        $("#preview-wrapper").removeClass('hidden');
        $.get('factura-xhr.html').success(function (rtext) {
            var tpl = _.template(rtext)
            var parsed = tpl(data)
            $("#preview-factura").html(parsed).find('table').footable();
        })
    }

    /*
     *  :::: FUNCTION :::: END
     */
    /*
     *  :::: SETUP :::: START
     */
    $tablaDocumento.footable();
    $tablaItem.footable();
    $tablaProducto.footable();
    $tablaDireccionesReceptor.footable();

    $('.modal').on('shown.bs.modal', function() {
        $(this).find('.footable').trigger('footable_initialize');
    })
    $modalDirecciones.on('click', '.agregar', function(e) {
        var direccion = $(this).attr('data-address');
        $txtLugarEntrega.val(direccion);
        $modalDirecciones.modal('hide');
    });
    $modalDirecciones.on('click', '.agregar-nuevo', function(e) {
        _direccionSeleccionada = $(this).closest('tr').attr('data-row-id');
        var direccion = _.find(_DireccionesReceptor, {codigo: _direccionSeleccionada})
        $txtLugarEntrega.val(direccion.domicilio);
        $modalDirecciones.modal('hide');
    });
    $tablaDocumento.on('click', 'button.remover', function(e) {
        var rowId = $(e.target).closest('tr').attr('data-row-id');
        eliminarDocumentoRelacionado({
            rowId: rowId
        });
    })
    $tablaItem.on('click', 'button.remover', function(e) {
        var rowId = $(e.target).closest('tr').attr('data-item-id');
        eliminarItem({
            itemId: rowId
        });
        refrescarResumen();
    })
    $tablaItem.on('click', 'button.editar', function(e) {
        var rowId = $(e.target).closest('tr').attr('data-item-id');
        var currItem = buscarItem({
            itemId: rowId
        });
        _itemEditando = currItem.itemId;
        popularModalItem(currItem);
        $modalItem.modal('show');
    })
    $btnOpenModalDR.on('click', function() {
        $formModalDocumentoRelacionado.find('.alert').remove();
        $formModalDocumentoRelacionado.find('.has-error').removeClass('has-error');
        $formModalDocumentoRelacionado.find(':input').val('');
        $modalDocumentoRelacionado.modal('show');
    })
    $btnOpenModalItem.on('click', function() {
        _itemEditando = false;
        $formModalItem.find('.alert').remove();
        $formModalItem.find('.has-error').removeClass('has-error');
        $formModalItem.find(':input').val('');
        $formModalItem.find('#txtCantidad').val((1).toMoney(2));
        $formModalItem.find('#txtDescuento').val((0).toMoney(2));
        $modalItem.modal('show');
    });
    $btnOpenModalSearch.on('click', function() {
        renderProductos();
        $modalSearch.modal('show');
    });
    $txtProductQuery.on('keyup', function(e) {
        var query = $txtProductQuery.val();
        var productos = filtarProductos(query);
        renderProductos(productos);
    });
    $tablaProducto.on('click', 'button.seleccionar', function(e) {
        var pid = $(e.target).closest('tr').attr('data-p-id');
        var producto = _.find(_Productos, {
            pId: pid
        });
        $formModalItem.find('#txtDescripcion').val(producto.txtDescripcion);
        $modalSearch.modal('hide');
    })
    
    $('[data-format-numeric]').on('change', function(e){
    	var places = e.target.dataset.decimalPlaces || 2;
    	e.target.value = $(this).numVal().toMoney(places);
    });

    $('.calculate-total').bind('change keyup', function(e) {
        var cantidad = $formModalItem.find('#txtCantidad').numVal();
        var valorUnidad = $formModalItem.find('#txtValorUnitario').numVal();
        var descuento = $formModalItem.find('#txtDescuento').numVal();
        var $igv = $formModalItem.find('#txtIGV');
        var $importe = $formModalItem.find('#txtImporte');
        var subtotal = (cantidad * valorUnidad) - descuento;
        var impuesto = 0.18 * subtotal;
        var total = impuesto + subtotal;
        $igv.val(impuesto.toMoney(2));
        $importe.val(total.toMoney(2));
    });
    $txtCodigo.add($txtDescripcion).on('typeahead:select', function(e, s) {
        var producto = _.find(_Productos, {
            txtCodigo: s.txtCodigo
        });
        if (producto) popularModalItem(producto, true, false);
    });
    $("#selTipo").on('change', function(e) {
        if (e.target.value == 'servicio') {
            $('#txtCantidad').val(1).attr('readonly', 'readonly');
        } else {
            $('#txtCantidad').removeAttr('readonly');
        }
    })
    var _CURR_DATE_PICKER = null;
    $('.datepicker').on('click', function(e) {
        _CURR_DATE_PICKER = $(e.target);
        $modalDatepicker.modal('show');
    })
    $datepicker.datepicker({
        // endDate: new Date(),
        startDate: new Date()
    }).on('changeDate', function(e) {
        $datepickerHidden.val(e.format());
    });
    $btnCloseDatepicker.on('click', function(e) {
        var selectedDate = $datepickerHidden.val();
        _CURR_DATE_PICKER.val(selectedDate).trigger('change');
        $datepickerHidden.val('');
        _CURR_DATE_PICKER = null;
        $modalDatepicker.modal('hide');
    });
    _.each(_tipoMedida, function(v, k) {
        $('<option/>', {
            value: k
        }).text(v).appendTo($selUnidadMedida);
    })
    _.each(_tipoProducto, function(v, k) {
            $('<option/>', {
                value: k
            }).text(v).appendTo($selTipo);
        })
    _.each(_monedaDescripcion, function (v, k) {
          $('<option/>', {
              value: k
          }).text(v).appendTo($selMoneda);
    })
    var ProductoCodigoBH = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('txtCodigo', 'txtDescripcion'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: [],
        identify: function(obj) {
            return obj.txtCodigo;
        },
    });
    $txtCodigo.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
          name: 'codigos',
          source: ProductoCodigoBH,
          templates: {
              suggestion: function(e) {
                  var par = $('<div/>')
                  $('<em/>').text(e.txtCodigo).appendTo(par);
                  par.append(' - ');
                  $('<span/>').text(e.txtDescripcion).appendTo(par);
                  return par;
              }
          },
        display: 'txtCodigo'
    });
    $txtDescripcion.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'codigos',
        source: ProductoCodigoBH,
        templates: {
            suggestion: function(e) {
                var par = $('<div/>')
                $('<em/>').text(e.txtCodigo).appendTo(par);
                par.append(' - ');
                $('<span/>').text(e.txtDescripcion).appendTo(par);
                return par;
            }
        },
        display: 'txtDescripcion'
    });
    $('.tt-query').css('background-color', '#fff');

    $RUCField.on('change', function () {
      $btnContinuar.addClass('hidden');
      $btnValidarDatosReceptor.removeClass('hidden');
      $RUCField.data('pivot-valid','1');
      $formDatosReceptor.valid();
    });
    
    $btnValidarDatosReceptor.on('click', function () {
    	if(!$RUCField.val())
    		return $formDatosReceptor.valid()

      // if($RUCField.data('validRUC') == '1'){
      if($RUCField.val()){
        $modalPreloader.modal('show');
        validarExistenciaRUC();
      }
    });

    $selMoneda.on('change', function (e) {
      $(".simbolo-moneda").html( _moneda[e.target.value] )
    })

    $btnMostrarModalItems.on('click', function (e) {
      if(!$selMoneda.val()){
        alert('seleccionar-moneda');
        return;
      }
      $modalListaItems.modal('show');
    });

    $btnMostrarModalDR.on('click', function (e) {
      $modalListaDocumentoRelacionado.modal('show');
    });

    $btnPreview.on('click', function (e) {
        if(!$formComprobante.valid())
            return;
        
        if(!$formItem.valid())
            return;
        
        showPreview();
    })

    $btnVolverFactura.on('click', function (e) {
        $('#root-panels').removeClass('hidden')
        $("#preview-wrapper").addClass('hidden');
    })
    /*
     *  :::: SETUP :::: END
     */
    /*
     *  :::: Form Validate :::: START
     */
    $formCatalogo.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            txtCodigo: {
                required: true,
                minlength: 11
            },
            txtDescripcion: {
                required: true
            }
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            txtCodigo: "Ingrese el codigo",
            txtDescripcion: "Ingrese la descripcion"
        },
        submitHandler: function(form) {
            form.submit();
            /*
            var url = '';
            $.ajax({
              method: 'post',
              dataType: 'json',
              complete: function () {
                window.location.href = "tabla-resultado.html"
              }
            })

            */
        }
    }));
    $formModalDocumentoRelacionado.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            selTipoDocumento: {
                required: true,
            },
            txtSerie: {
                required: true,
            },
            txtNumero: {
                required: true,
            },
            txtDescripcion: {
                required: true,
            },
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            selTipoDocumento: {
                required: 'required: -> selTipoDocumento'
            },
            txtSerie: {
                required: 'required: -> txtSerie'
            },
            txtNumero: {
                required: 'required: -> txtNumero'
            },
            txtDescripcion: {
                required: 'required: -> txtDescripcion'
            },
        },
        submitHandler: function(form) {
            var data = $(form).first().serializeObject();
            data.rowId = _.uniqueId('dr-');
            agregarDocumentoRelacionado(data);
            $modalDocumentoRelacionado.modal('hide');
        }
    }))
    $formModalItem.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        rules: {
            txtCantidad: {
                required: true,
                lessThan: 10
            },
            selUnidadMedida: {
                required: true,
            },
            txtCodigo: {
                required: true,
                maxlength: 15
            },
            txtDescripcion: {
                required: true,
                maxlength: 200,
            },
            txtValorUnitario: {
                required: true
            },
            txtDescuento: {
                required: true
            },
            selTipo: {
                required: true,
            }
        },
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            txtCantidad: {
                required: 'required: -> txtCantidad',
                lessThan: 'lessThan: -> txtCantidad'
            },
            selUnidadMedida: {
                required: 'required: -> selUnidadMedida',
            },
            txtCodigo: {
                required: 'required: -> txtCodigo',
            },
            txtDescripcion: {
                required: 'required: -> txtDescripcion',
            },
            txtValorUnitario: {
                required: 'required: -> txtValorUnitario',
            },
            txtDescuento: {
                required: 'required: -> txtDescuento',
            },
            txtIGV: {
                required: 'required: -> txtIGV',
            },
            txtImporte: {
                required: 'required: -> txtImporte',
            },
            selTipo: {
                required: 'required: -> selTipo',
            }
        },
        submitHandler: function(form) {
            var data = $(form).last().serializeObject();
            if (_itemEditando) {
                editarItem({
                    itemId: _itemEditando
                }, data);
            } else {
                data.itemId = _.uniqueId('it-');
                agregarItem(data);
            }
            $modalItem.modal('hide');
            _itemEditando = false;
            refrescarResumen();
        }
    }));

    $formDatosReceptor.validate(_.extend(window._validatorWallSettings, {
            debug: true,
            /*onkeyup: false,
            onchange: false,
            onclick: false,
            onfocusout: false,*/
            rules: {
                txtRuc: {
                    required: true,
                 /*   minlength: 11,
                    validRUC: true,
                    maxlength: 11,
                    validpivot: true*/
                }
            },
            // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
            messages: {},
            submitHandler: function(form) {
                $btnContinuar.addClass('hidden');
                $RUCField.attr('disabled', 'disabled')
                $panelDatosReceptor.find('.panel-collapse').collapse('hide');
                $panelDatosComprobante.removeClass('hidden').find('.panel-collapse').collapse('show');
                $panelDatosComprobante.removeClass('hidden').find('.panel-collapse').collapse('show');
                $panelDocumentosRelacionados.removeClass('hidden').find('.panel-collapse').collapse('show');
                $panelItems.removeClass('hidden').find('.panel-collapse').collapse('show');
                $panelResumen.removeClass('hidden').find('.panel-collapse').collapse('show');

                $("#buttons").removeClass('hidden');
                renderDireccionesReceptor();
                prefetchProductos();
                renderDocumentoRelacionado()
                renderItem();
                refrescarResumen();
            }
        }));

        $formItem.validate(_.extend(window._validatorWallSettings, {
            rules: {
                hiddenHelperItem: {
                    moreThan: 1,
                    lessThan: 10
                }
            },
            ignore: '.ignored-field',
            messages: {
                hiddenHelperItem: {
                    moreThan: "Agregue un item a la factura",
                    lessThan: "Solo se permiten 10 items en la factura."
                }
            }
        }))

        $formComprobante.validate(_.extend(window._validatorWallSettings, {
            rules: {
                selMoneda: {
                    required: true
                },
                txtLugarEntrega: {
                    required: true
                }
            },
            messages: {
                selMoneda: {
                    required: "required: selMoneda"
                },
                txtLugarEntrega: {
                    required: "required: txtLugarEntrega"
                }
            }
        }));
        
        
        /*
         *  :::: Form Validate :::: END
         */
// })(window, jQuery, _);