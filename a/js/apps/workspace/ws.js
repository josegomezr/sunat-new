/*Funciones m�vil v13-02-2015 1300*/
/*

Ubicaci�n: /a/js/apps/workspace/ws.js 

 */
var esAndroid=false;
var esIos=false;
var esMovil=false;
var movilUrl='url';
var ag=navigator.userAgent.toLowerCase();
if (ag.indexOf("android") >= 0) {
	esAndroid = true;
}
if (ag.indexOf("iphone") >= 0 || ag.indexOf("ipad") >= 0) {
	esIos = true;
}
/*
if (ag.indexOf("android") >= 0 || ag.indexOf("iphone") >= 0 || ag.indexOf("ipad") >= 0) {
	esMovil = true;
}*/
$( document ).ready(function() {   	
	$("#btnVolverWorkspace").hide();
	frameIniciado();
});
function iniciaMovil(){
	esMovil=true;
	
	//margenAbajo();
	
	evaluaLogo(false);
	
	
	movilUrl=window.location.href;


	 $("input[type='text'],input[type='number'],input[type='password'],input[type='tel'],input[type='email'], textarea").on('focus', function(event){
		 if(esAndroid){
			margenAbajo();
			var resultado=isElementInViewportHalf(this);
			if(resultado!=true){
				var ele=$(this);
				setTimeout(function(){ 
					$('html, body').animate({
						scrollTop: (ele.offset().top-100)
					}, 500);
				 }, 100);
			}
		 }    		
	 });  
	 
	 loginEventos();

	 $("#btnVolverWorkspace").show();
	 
	 //irArriba();
	 setTimeout(function(){ 		
		frameCargado();
	 }, 200);
}
function loginEventos(){
	$("#divSignonFormLogo").hide();
	$("#divSignonFormFooter").hide();
	$("#divSignonFormCandado").hide();
	$("#aOlvidaste").hide();
	
	$("#txtUsuario").removeAttr('maxlength');
	$("#txtContrasena").removeAttr('maxlength');
	$("#txtRuc").removeAttr('maxlength');
	
}

function listenerEventos(event){
	if (event.data == 'evaluaLogo') {
		evaluaLogo(false);
	}else if (event.data == 'irArriba') {
		irArriba();
	}else if (event.data == 'esAndroid') {
		esAndroid=true;
	}else if (event.data == 'margenAbajo') {
		margenAbajo();
	}else if (event.data == 'mensajeVolver') {
		mensajeVolver();
	}else if (event.data == 'irHome') {
		irHome();
	}else if (event.data == 'irHomeRhe') {
		irHomeRhe();
	}else if (event.data == 'iniciaMovil') {
		iniciaMovil();
	}else if (event.data == 'anulaMargenAbajoWs') {
		anulaMargenAbajoWs();
	}else if (event.data == 'pingWs') {
		pingWs();
	}
}
if(window.addEventListener){
	addEventListener("message", listenerEventos, false);
}else{
	attachEvent("onmessage", listenerEventos);
}

 
 function margenAbajo(){
	var mitadAlto=parseInt(window.innerHeight/2,10);   
	if(mitadAlto<100){
		mitadAlto=parseInt($("body").height()/3,10);
	}
	$("body").css("margin-bottom",''+mitadAlto+'px'); 
 }
 
 function anulaMargenAbajoWs(){
	$("body").css("margin-bottom",'0px !important');   
	/*if (!$(this).find("input").length) {
        alert("form empty!");
        return false;
    }*/
 }
 function isElementInViewportHalf(el) {
	var rect = el.getBoundingClientRect();
	return rect.bottom > 0 &&
		rect.right > 0 &&
		rect.left < (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */ &&
		rect.top < (parseInt(window.innerHeight/4,10) || parseInt(document.documentElement.clientHeight/4,10)) /*or $(window).height() */;
 }	 
 function evaluaLogo(dato){
	 if(dato==true){
	 }else{
		 $("#divHeader").hide();
	 }
 }
 function irArriba(){
	 var top = $('body').scrollTop();
	 if(parseInt(top, 10)>0){
		 $('html, body').animate({
			 scrollTop: 0
		 }, 450);
		 
	 }
	 if(esAndroid){
		$('#divHeader').after('<input type="checkbox" id="focusable" style="height:0; margin-left:-200px; clear: both;" />');
		$('#focusable').focus();
		$('#focusable').remove();
	 }
 }
 function frameIniciado(){
	try{
		parent.postMessage("frameIniciado",'*');
	}catch(ex){
		try{
			window.parent.postMessage("frameIniciado",'*');
		}catch(ex){
		}
	}
	
	
	 try{
		
		if(esAndroid){
			$("#btnVolverAndroid").removeClass("hidden");					
			$("#btnVolverAndroid").bind('click',function(event){
				history.back();
				
				event.preventDefault();
				event.stopImmediatePropagation();
				return false;
			});   

			var btn1=$(".btnVolverAndroid");
			btn1.removeClass("hidden");					
			btn1.bind('click',function(event){
				history.back();
				
				event.preventDefault();
				event.stopImmediatePropagation();
				return false;
			});   

			
			$(".btnVolverAntiguo").addClass("hidden");//rhe	
			$("#btn_salir").addClass("hidden");//denuncias	
			
		}else if(esIos){		
			$("#btnVolverIos").removeClass("hidden");					
			$("#btnVolverIos").bind('click',function(event){
				//TODO para ios
				
				event.preventDefault();
				event.stopImmediatePropagation();
				return false;
			}); 

			
			var btn2=$(".btnVolverIos");
			btn2.removeClass("hidden");					
			btn2.bind('click',function(event){
				//TODO para ios
				
				event.preventDefault();
				event.stopImmediatePropagation();
				return false;
			}); 			

			$(".btnVolverAntiguo").addClass("hidden");//rhe	
			$("#btn_salir").addClass("hidden");//denuncias	
		}
		
	}catch(ex){
	}
}
 function frameCargado(){
	try{
		parent.postMessage("frameCargado",'*');
	}catch(ex){
		try{
			window.parent.postMessage("frameCargado",'*');
		}catch(ex){
		}
	}
}
 function pingWs(){
	try{
		parent.postMessage("respuestaPingWs",'*');
	}catch(ex){
		try{
			window.parent.postMessage("respuestaPingWs",'*');
		}catch(ex){
		}
	}
}
function regresaWorkspace(){
	try{
		parent.postMessage("regresaWorkspace",'*');
	}catch(ex){
		try{
			window.parent.postMessage("regresaWorkspace",'*');
		}catch(ex){
		}
	}
}
function refrescaFrameWs(){
	try{
		parent.postMessage("refrescaFrameWs",'*');
	}catch(ex){
		try{
			window.parent.postMessage("refrescaFrameWs",'*');
		}catch(ex){
		}
	}
}
function mensajeVolver(){
	try{
		regresa();
	}catch(ex){
	}
	
	try{
		if(esAndroid){				
			$("#btnVolverAndroid").trigger('click');
			$(".btnVolverAndroid").trigger('click');
		}else if(esIos){					
			$("#btnVolverIos").trigger('click');
			$(".btnVolverIos").trigger('click');
		}
		
	}catch(ex){
	}
	
}


$("#btnNuevaConsultaRhe").bind('click',function(event){
	var href = $("#aNuevaConsultaRhe").attr('href');
	window.location.href = href;
	
	
	event.preventDefault();
	event.stopImmediatePropagation();
	return false;
});    


function irHomeRhe(){
	$("#btnNuevaConsultaRhe").trigger('click');
}

/*
divSignonFormLogo
divSignonFormFooter
divSignonFormCandado
aOlvidaste

h1{
	font-size: 1.7em;
}
.header div{
	padding-left: 0 !important;
} 
.imgLogo{
	height: 2.5em;
	margin-top: 1em;
}
.blockUI h1{
	font-size: 12px !important;
}
*/
/*Fin funciones m�vil*/