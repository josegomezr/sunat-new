(function (window, $, _) {
    /*
     * :::: CONTAINERS :::: START
     */
    var $formCatalogo = $("#form-catalogo");
    var $txtCodigo = $formCatalogo.find("#txtCodigo");
    var $txtDescripcion = $formCatalogo.find("#txtDescripcion");
    var $_ambosValido = $formCatalogo.find("#ambosValido");
    var $formRegistroProductos = $("#form-modal-registro-productos");
    var $txtCodigo_prod = $formRegistroProductos.find("#txtCodigo");
    var $txtDescripcion_prod = $formRegistroProductos.find("#txtDescripcion");
    var $selTipo_prod = $formRegistroProductos.find("#selTipo");
    var $selUnidad_prod = $formRegistroProductos.find("#selUnidad");
    var $txtPen_prod = $formRegistroProductos.find("#txtPen");
    var $txtUsd_prod = $formRegistroProductos.find("#txtUsd");
    var $txtEur_prod = $formRegistroProductos.find("#txtEur");
    var $txtGbp_prod = $formRegistroProductos.find("#txtGbp");
    var $txtYen_prod = $formRegistroProductos.find("#txtJpy");
    var $txtChf_prod = $formRegistroProductos.find("#txtChf");
    var $txtSec_prod = $formRegistroProductos.find("#txtSek");
    var $txtCad_prod = $formRegistroProductos.find("#txtCad");
    var $btnNuevoProducto = $("#btnNuevoProducto");
    var $formProducto = $(".form-crud-productos");
    var $txtCodigo_edit = $formProducto.find("#txtCodigo");
    var $txtDescripcion_edit = $formProducto.find("#txtDescripcion");
    var $selTipo_edit = $formProducto.find("#selTipo");
    var $selUnidad_edit = $formProducto.find("#selUnidad");
    var $txtPen_edit = $formProducto.find("#txtPen");
    var $txtUsd_edit = $formProducto.find("#txtUsd");
    var $txtEur_edit = $formProducto.find("#txtEur");
    var $txtGbp_edit = $formProducto.find("#txtGbp");
    var $txtYen_edit = $formProducto.find("#txtJpy");
    var $txtChf_edit = $formProducto.find("#txtChf");
    var $txtSec_edit = $formProducto.find("#txtSek");
    var $txtCad_edit = $formProducto.find("#txtCad");
    var $seccionOtrasMonedas = $('.otras-monedas');
    var $btnEliminar = $(".remover");
    var $btnEditar = $(".editar");
    var $modalEditarProducto = $("#modal-editar-producto");
    var $modalRegistroProducto = $("#modal-registro-productos");
    /// quickfix 14-10-2015
    function algunInputMonedaMenos(self) {
        var moneyInputs = $(".square-addon").parent().find('input');
        if (self) moneyInputs = moneyInputs.not(self);
        return [].some.call(moneyInputs, function (e) {
            return !!e.value
        });
    }
    /// end - quickfix 14-10-2015
    var _validateRegistroEditarSetup = {
        rules: {
            txtCodigo: {
                required: true,
                maxlength: 15,
                validStr: true
            },
            txtDescripcion: {
                required: true,
                maxlength: 200,
                validStr: true
            },
            selTipo: {
                required: true,
            },
            selUnidad: {
                required: true,
            },
            /// quickfix 14-10-2015
            txtPen: {
                required: function () {
                    return !algunInputMonedaMenos('#txtPen');
                },
                number: true,
                lessThan: function () {
                    return !algunInputMonedaMenos('#txtPen') ? 999999999999.9999999999 : false;
                },
                moreThan: function () {
                    return !algunInputMonedaMenos('#txtPen') ? 0 : false;
                }
            },
            txtUsd: {
                number: true,
            },
            txtEur: {
                number: true,
            },
            txtCad: {
                number: true,
            },
            txtGbp: {
                number: true,
            },
            txtJpy: {
                number: true,
            },
            txtSek: {
                number: true,
            },
            txtChf: {
                number: true,
            },
            /// end - quickfix 14-10-2015
        },
        messages: {
            txtCodigo: {
                required: "Ingrese el c&oacute;digo",
                maxlenght: "S&oacute;lo se permiten 15 caracteres para el c&oacute;digo",
                validStr: "El formato del c&oacute;digo no es v&aacute;lido",
            },
            txtDescripcion: {
                required: "Ingrese la descripci&oacute;n",
                maxlenght: "S&oacute;lo se permiten 200 caracteres para la descripci&oacute;n",
                validStr: "El formato de la descripci&oacute;n no es v&aacute;lido",
            },
            selTipo: {
                required: "Seleccione el tipo de servicio"
            },
            selUnidad: {
                required: "Seleccione la unidad de medida"
            },
            txtPen: {
                required: "Ingrese un valor en el campo moneda",
                moreThan: "El monto ingresado es incorrecto"
            },
        }
    }
    $('.square-addon').on('click', function (e) {
        $(e.target).removeClass('disabled');
    })
    $('.disable-currency').on('click', function (e) {
        $(e.target).closest('.btn').parent().find('.square-addon').addClass('disabled');
    })
    $txtCodigo.add($txtDescripcion).on('change keyup', function (e) {
        $_ambosValido.val((!!$txtCodigo.val() | !!$txtDescripcion.val()) ? "valid" : "").trigger('change');
    })
    $btnEliminar.on('click', function (e) {
        if (!confirm('¿Seguro de eliminar el registro seleccionado?')) {
            e.preventDefault();
            return;
        }
        id = $(this).closest('tr').attr('id');
        $(this).closest('tr').remove();
        var url = "catalogo.do?action=eliminarCatalogo";
        var data = {
            txtCodigo: id
        }
        $.post(url, data).success(function (data) {
            window.location.href = "catalogo.do";
        }).error(function (reason) {
            alert('Disculpe, su producto no ha podido ser eliminado, intentelo nuevamente.');
        });
    })
    $btnNuevoProducto.on('click', function (e) {
        $modalRegistroProducto.find(':input').val('');
    })
    $btnEditar.on('click', function (e) {
        codigo = $(this).attr('data-codigo');
        descripcion = $(this).attr('data-descripcion');
        tipo = $(this).attr('data-tipo');
        unidad = $(this).attr('data-unidad');
        pen = $(this).attr('data-pen');
        eur = $(this).attr('data-eur');
        usd = $(this).attr('data-usd');
        gbp = $(this).attr('data-lib');
        jpy = $(this).attr('data-yen');
        chf = $(this).attr('data-cor');
        sec = $(this).attr('data-fra');
        cad = $(this).attr('data-cad');
        $txtCodigo_edit.val(codigo);
        $txtDescripcion_edit.val(descripcion);
        $selTipo_edit.val(tipo);
        $selUnidad_edit.val(unidad);
        $txtPen_edit.val(pen);
        $txtEur_edit.val(eur);
        $txtUsd_edit.val(usd);
        $txtGbp_edit.val(gbp);
        $txtYen_edit.val(jpy);
        $txtChf_edit.val(chf);
        $txtSec_edit.val(sec);
        $txtCad_edit.val(cad);
    })
    $('.footable').footable();
    $('.mostrar-monedas').on('click', function (e) {
        var $self = $(this).closest('.btn'); // .parent().parent().addClass('hidden');
        var $icon = $self.find('i')
        if ($icon.hasClass('glyphicon-chevron-down')) $icon.addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down')
        else $icon.addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up')
        $seccionOtrasMonedas.toggleClass('hidden');
    })
    $formCatalogo.validate(_.extend(window._validatorWallSettings, {
        debug: true,
        ignore: 'button',
        rules: {
            txtCodigo: {
                maxlength: 15,
                minlength: 1,
                validStr: true
            },
            txtDescripcion: {
                maxlength: 200,
                minlength: 3,
                validStr: true
            },
            ambosValido: {
                required: true
            }
        },
        // el minlength de codigo es 1  y del de descripcion es 3 
        // estos mensajes estan aqui porque son d&iacute;n&aacute;micos respecto al html
        messages: {
            ambosValido: {
                required: "Por favor ingrese un criterio de b&uacute;squeda",
            },
            txtCodigo: {
                maxlength: "El n&uacute;mero m&aacute;ximo de caracteres es 15",
                minlength: "El n&uacute;mero m&iiacute;nimo de caracteres es 1",
                validStr: "Verifique el c&oacute;digo",
            },
            txtDescripcion: {
                maxlength: "El n&uacute;mero m&aacute;ximo de caracteres ingresados es 200",
                minlength: "El n&uacute;mero m&iacute;nimo de caracteres es 3",
                validStr: "Verifique la descripci&oacute;n"
            }
        },
        submitHandler: function (form) {
            console.log('valid-consulta-catalogo')
            form.submit();
        }
    }));
    
    function realizarRegistroProducto (formData) {
        return realizarConsulta('catalogo.do?action=agregarCatalogo', formData);
    }

    function realizarEdicionProducto (formData) {
        return realizarConsulta('catalogo.do?action=modificarCatalogo', formData);
    }

    function realizarConsulta (url, data) {
        var dfd = jQuery.Deferred();
        $.ajax({
            url: url,
            method: 'post',
            dataType: 'json',
            data: data,
            success: function (data) {
                dfd.resolve( data );
            },
            error: function (jqXhr, textStatus, errorThrown) {
                dfd.reject( {
                    message: 'Disculpe, su producto no ha podido ser registrado, intentelo nuevamente.'
                } );
            }
        });
        return dfd;
    }

    $formProducto.validate(_.extend(window._validatorWallSettings, _validateRegistroEditarSetup, {
        debug: true,
        submitHandler: function (form) {
            var $form =  $(form);
            var formData = $form.serializeObject();
            var promise;
            if (formData._OP_ == 'create') {
                promise = realizarRegistroProducto(formData)
            }else{
                promise = realizarEdicionProducto(formData)
            }
            promise.then(function (result) {
                // callback para la consulta realizada
                // por consistencia yo llamaría al mensaje result.message
                // alert(result.success);
                alert(result.success);
                window.location.href = "catalogo.do";
            }).fail(function (reason) {
                // callback para los errores
                alert(reason.message);
            });
        }
    }));

    function soloNumeros(e) {
        var key = window.Event ? e.which : e.keyCode
        return (key >= 48 && key <= 57)
    }

    function soloLetrasNumerosEspacios(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";
        especiales = "8-37-39-46";
        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function SoloNumeroDecimal(e, field) {
        key = e.keyCode ? e.keyCode : e.which
            // backspace
        if (key == 8) return true
            // 0-9 a partir del .decimal  
        if (field.value != "") {
            if ((field.value.indexOf(".")) > 0) {
                //si tiene un punto valida dos digitos en la parte decimal
                if (key > 47 && key < 58) {
                    if (field.value == "") return true
                        //regexp = /[0-9]{1,10}[\.][0-9]{1,3}$/
                    regexp = /[0-9]{2}$/
                    return !(regexp.test(field.value))
                }
            }
        }
        // 0-9 
        if (key > 47 && key < 58) {
            if (field.value == "") return true
            regexp = /[0-9]{10}/
            return !(regexp.test(field.value))
        }
        // .
        if (key == 46) {
            if (field.value == "") return false
            regexp = /^[0-9]+$/
            return regexp.test(field.value)
        }
        // other key
        return false
    }
    //03:08 27/09/2015
})(window, jQuery, _);